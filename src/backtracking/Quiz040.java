package backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
40. Combination Sum II
Given a collection of candidate numbers (C) and a target number (T), find all unique combinations in C
where the candidate numbers sums to T.

Each number in C may only be used once in the combination.

Note:
All numbers (including target) will be positive integers.
The solution set must not contain duplicate combinations.
For example, given candidate set [10, 1, 2, 7, 6, 1, 5] and target 8,
A solution set is:
[
  [1, 7],
  [1, 2, 5],
  [2, 6],
  [1, 1, 6]
]
 */

public class Quiz040 {
    /**
     * 思路：回溯
     * 因为只能用一次，需要考虑重复元素的情况比如 {1,1,2,5} target 为 8，那么会出现两个 {1,2,5}
     * 在第一层 1 -> {1,2,5}  1 -> {2, 5} 属于重复结果，1 -> {1,2,5} 递归考察完 1->1 ... 这条路径并恢复现场后就是 1->{2,5} 这种情况了
     * 要解决这种问题可以判断下当前元素是否和前一个相同
     *
     * 可以对比下LeetCode39的combination sum，前者没有重复元素，且每个元素都可以用无限次
     */
    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        List<List<Integer>> ans = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        Arrays.sort(candidates);
        backtrack(ans, path, candidates, target, 0);
        return ans;
    }

    public void backtrack(List<List<Integer>> ans, List<Integer> path, int[] candidates, int remain, int curIndex) {
        if(remain < 0) {
            return;
        } else if(remain == 0) {
            ans.add(new ArrayList<>(path));
        } else {
            for(int i= curIndex; i<candidates.length; i++) {
                //同一层过滤重复元素
                if(i > curIndex && candidates[i] == candidates[i-1]) {
                    continue;
                }
                path.add(candidates[i]);
                //每个元素只能用一次，所以curIndex = i + 1 即从下一个元素开始考察
                backtrack(ans, path, candidates, remain - candidates[i], i + 1);
                path.remove(path.size() - 1);
            }
        }
    }
}
