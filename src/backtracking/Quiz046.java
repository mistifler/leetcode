package backtracking;

import java.util.ArrayList;
import java.util.List;

/*
46. Permutations

Given a collection of numbers, return all possible permutations.

For example,
[1,2,3] have the following permutations:
[1,2,3], [1,3,2], [2,1,3], [2,3,1], [3,1,2], and [3,2,1].
 */
public class Quiz046 {

    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> ans = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        backtrack(nums, ans, path);
        return ans;
    }

    public void backtrack(int[] nums, List<List<Integer>> ans, List<Integer> path) {
        if(path.size() == nums.length) {
            ans.add(new ArrayList<>(path));
            return;
        }

        for(int num : nums) {
            if(path.contains(num)) {
                continue;
            }
            path.add(num);
            backtrack(nums, ans, path);
            path.remove(path.size() - 1);
        }
    }

}
