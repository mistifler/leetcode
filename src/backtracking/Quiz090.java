package backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
90. Subsets II
Given a collection of integers that might contain duplicates, nums, return all possible subsets (the power set).

Note: The solution set must not contain duplicate subsets.

For example,
If nums = [1,2,2], a solution is:

[
  [2],
  [1],
  [1,2,2],
  [2,2],
  [1,2],
  []
]
 */

public class Quiz090 {
    public List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(list, new ArrayList<>(), nums, 0);
        return list;
    }

    /**
     * 回溯：
     * 参考LeetCode78 subset，每次选择的时候需要看是否和前一个元素相同，不然会有重复的结果
     *
     */
    private void backtrack(List<List<Integer>> list , List<Integer> path, int [] nums, int start){
        list.add(new ArrayList<>(path));
        System.out.println(path.toString());
        for(int i = start; i < nums.length; i++){
            if(i > start && nums[i] == nums[i-1]) continue;
            path.add(nums[i]);
            backtrack(list, path, nums, i + 1);
            path.remove(path.size() - 1);
        }
    }
}
