package backtracking;

/*
47. Permutations II
Given a collection of numbers that might contain duplicates, return all possible unique permutations.

For example,
[1,1,2] have the following unique permutations:
[
  [1,1,2],
  [1,2,1],
  [2,1,1]
]

 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Quiz047 {
    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> ans = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        boolean[] used = new boolean[nums.length];
        Arrays.sort(nums);
        backtrack(ans, path, nums, used);
        return ans;
    }

    public void backtrack(List<List<Integer>> ans, List<Integer> path, int[] nums, boolean[] used) {
        if(path.size() == nums.length) {
            ans.add(new ArrayList<>(path));
        }

        for(int i=0; i<nums.length; i++) {
            if(used[i] || i>0 && nums[i] == nums[i-1] && !used[i-1]) {
                continue;
            }
            path.add(nums[i]);
            used[i] = true;
            backtrack(ans, path, nums, used);
            path.remove(path.size() - 1);
            used[i] = false;
        }
    }
}
