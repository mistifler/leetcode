package backtracking;

/*
79. Word Search
Given a 2D board and a word, find if the word exists in the grid.

The word can be constructed from letters of sequentially adjacent cell, where "adjacent" cells are those horizontally or vertically neighboring. The same letter cell may not be used more than once.

For example,
Given board =

[
  ['A','B','C','E'],
  ['S','F','C','S'],
  ['A','D','E','E']
]
word = "ABCCED", -> returns true,
word = "SEE", -> returns true,
word = "ABCB", -> returns false.
 */
public class Quiz079 {
    /**
     * 思路：回溯
     * 起始位置是任意的，所以需要遍历整个二维数组，以每个字符为开始进行尝试
     * visited数组用于记录字符是否被访问过,如果board可以被修改，也可以直接把board改成其他字符用于标记，这样可以省去这个空间复杂度。还有就是记得用完清理现场
     * 如果出现以下三种情况直接返回false，否则就往四个方向考察
     * 越界
     * 字符不匹配
     * 已经被访问过，比如{{'a','a'}} "aaa" ，如果不标记访问过的话，这个会返回true
     */
    static boolean[][] visited;

    public boolean exist(char[][] board, String word) {
        visited = new boolean[board.length][board[0].length];
        for (int i = 0; i < board.length; ++i) {
            for (int j = 0; j < board[0].length; ++j) {
                if (board[i][j] == word.charAt(0) && backtrack(board, i, j, word, 0)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * x y 表示board遍历到的位置，cur表示word遍历到的位置
     */
    public boolean backtrack(char[][] board, int x, int y, String word, int cur) {
        if (word.length() == cur) {
            return true;
        }

        //如果越界，或者字符不相等，或者已经被访问过都直接返回false
        if (x < 0 || y < 0 || x >= board.length || y >= board[0].length ||
                board[x][y] != word.charAt(cur) ||
                visited[x][y]) {
            return false;
        }

        visited[x][y] = true;
        if (backtrack(board, x + 1, y, word, cur + 1) ||
                backtrack(board, x - 1, y, word, cur + 1) ||
                backtrack(board, x, y + 1, word, cur + 1) ||
                backtrack(board, x, y - 1, word, cur + 1)) {
            return true;
        }
        //使用完记得清理现场，防止污染其他路径上的搜索
        visited[x][y] = false;
        return false;
    }

    public static void main(String[] args) {
        char[][] board = {{'a', 'b'}};
        String word = "ba";
    }

}
