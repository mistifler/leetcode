package backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
78. Subsets
Given a set of distinct integers, nums, return all possible subsets (the power set).

Note: The solution set must not contain duplicate subsets.

For example,
If nums = [1,2,3], a solution is:

[
  [3],
  [1],
  [2],
  [1,2,3],
  [1,3],
  [2,3],
  [1,2],
  []
]
 */
public class Quiz078 {
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(list, new ArrayList<>(), nums, 0);
        return list;
    }

    /**
     * 回溯：
     * 需要保存所有的路径，所以一开始需要将当前path直接放入到结果
     * 每选中一个元素，然后需要递归的走剩下的元素，每个元素选中后还需要从路径中清除
     *
     * 比如数组{1,2,3}各个子集被放入结果集的顺序如下：
     * [] [1] [1, 2] [1, 2, 3] [1, 3] [2] [2, 3] [3]
     *
     * 可以认为是LeetCode39的简化版，因为不需要实时去计算当前path所保存的信息
     */
    private void backtrack(List<List<Integer>> list , List<Integer> path, int [] nums, int start){
        list.add(new ArrayList<>(path));
        for(int i = start; i < nums.length; i++){
            path.add(nums[i]);
            backtrack(list, path, nums, i + 1);
            path.remove(path.size() - 1);
        }
    }

    public static void main(String[] args) {
        int[] nums = {1,2,3};
        Quiz078 quiz078 = new Quiz078();
        quiz078.subsets(nums);
    }

}
