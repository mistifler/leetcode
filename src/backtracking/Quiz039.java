package backtracking;

/*
39. Combination Sum
Given a set of candidate numbers (C) (without duplicates) and a target number (T),
find all unique combinations in C where the candidate numbers sums to T.

The same repeated number may be chosen from C unlimited number of times.

Note:
All numbers (including target) will be positive integers.
The solution set must not contain duplicate combinations.
For example, given candidate set [2, 3, 6, 7] and target 7,
A solution set is:
[
  [7],
  [2, 2, 3]
]

 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Quiz039 {
    /**
     * 思路：排序后回溯
     * 函数参数保存离目标值还差多少 remain，和当前考察的起始元素位置信息 curIndex
     * 递归结束条件：
     *   如果小于0递归结束，当前path没有找到结果
     *   如果等于0递归结束，当前path记录了一条结果，放入全局结果
     * 否则，需要继续考察：
     *   循环遍历，因为可以无限使用，所以下层递归依然是从当前元素开始考察
     */
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> ans = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        Arrays.sort(candidates);
        backtrack(ans, path, candidates, target, 0);
        return ans;
    }

    public void backtrack(List<List<Integer>> ans, List<Integer> path, int[] candidates, int remain, int curIndex) {
        if (remain < 0) {
            return;
        } else if (remain == 0) {
            ans.add(new ArrayList<>(path));
            return;
        } else {
            for (int i = curIndex; i < candidates.length; i++) {
                path.add(candidates[i]);
                //下次依然从当前位置考察
                backtrack(ans, path, candidates, remain - candidates[i], i);
                //恢复现场
                path.remove(path.size() - 1);
            }
        }
    }

    public static void main(String[] args) {
        Quiz039 quiz039 = new Quiz039();
        int[] test = {2, 3, 6, 7};
        System.out.println(quiz039.combinationSum(test, 7));
    }
}
