package backtracking;

/*
131. Palindrome Partitioning
Given a string s, partition s such that every substring of the partition is a palindrome.

Return all possible palindrome partitioning of s.

For example, given s = "aab",
Return

[
  ["aa","b"],
  ["a","a","b"]
]
 */

import java.util.ArrayList;
import java.util.List;

public class Quiz131 {
    public List<List<String>> partition(String s) {
        List<List<String>> ans = new ArrayList<>();
        List<String> path = new ArrayList<>();
        palindromPartition(ans, path, s, 0);
        return ans;
    }

    public void palindromPartition(List<List<String>> ans, List<String> path, String s, int start) {
        if(start == s.length()) {
            ans.add(new ArrayList<>(path));
            return;
        }
        for(int i=start; i<s.length(); i++) {
            if(isPalindrome(s, start, i)) {
                //注意substr是不包括endIdx本身的
                path.add(s.substring(start, i + 1));
                palindromPartition(ans, path, s, i + 1);
                path.remove(path.size() - 1);
            }
        }
    }

    public boolean isPalindrome(String s, int start, int end) {
        while(start < end) {
            if(s.charAt(start++) != s.charAt(end--)) {
                return false;
            }
        }
        return true;
    }
}
