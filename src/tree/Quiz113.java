package tree;

/*
  113. Path Sum II
  Given a binary tree and a sum, find all root-to-leaf paths where each path's sum equals the given sum.

  For example:
  Given the below binary tree and sum = 22,
              5
             / \
            4   8
           /   / \
          11  13  4
         /  \    / \
        7    2  5   1
  return
    [
       [5,4,11,2],
       [5,8,4,5]
    ]
 */

import java.util.ArrayList;
import java.util.List;

public class Quiz113 {
    /**
     * 思路：dfs
     * 用一个List保存访问路径，先把当前节点加入，然后深搜左右子树，搜完要记得把节点删除，不然会被错误带到下一次路径中
     */
    List<List<Integer>> ans = new ArrayList<>();
    public List<List<Integer>> pathSum(TreeNode root, int sum) {
        List<Integer> list = new ArrayList<>();
        dfs(root, list, sum);
        return ans;
    }

    public void dfs(TreeNode root, List<Integer> ret, int sum) {
        if(root == null) {
            return;
        }
        ret.add(root.val);
        if(root.left == null && root.right == null && root.val == sum) {
            ans.add(new ArrayList<>(ret));
        } else {
            dfs(root.left, ret, sum - root.val);
            dfs(root.right, ret, sum - root.val);
        }

        ret.remove(ret.size() - 1);
    }
}
