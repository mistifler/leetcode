package tree;

/*
  173. Binary Search Tree Iterator
  Implement an iterator over a binary search tree (BST).
  Your iterator will be initialized with the root node of a BST.

  Calling next() will return the next smallest number in the BST.

  Note: next() and hasNext() should run in average O(1) time and uses O(h) memory,
  where h is the height of the tree.

  Your BSTIterator will be called like this:
     BSTIterator i = new BSTIterator(root);
     while (i.hasNext()) v[f()] = i.next();
 */

import java.util.Stack;

public class Quiz173 {

    /**
     * 思路：核心是栈的使用
     * 因为要求高度为树的高度，所以提前先序遍历完存入List的做法不可取
     * 这里可以用一个栈来存储节点的引用,当调用next方法的时候如果发现还有右子树，就进一步的将右子树放入栈中，这样可以保持栈不超过O(h)
     */
    public class BSTIterator {

        private Stack<TreeNode> stack = new Stack<>();

        public BSTIterator(TreeNode root) {
            pushLeft(root);
        }

        public void pushLeft(TreeNode root) {
            while(root != null) {
                stack.push(root);
                root = root.left;
            }
        }

        /** @return whether we have a next smallest number */
        public boolean hasNext() {
            return !stack.isEmpty();
        }

        /** @return the next smallest number */
        public int next() {
            if(stack.isEmpty()) {
                return -1;
            }
            TreeNode cur = stack.pop();
            if(cur.right != null) {
                pushLeft(cur.right);
            }
            return cur.val;
        }
    }
}


