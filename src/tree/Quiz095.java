package tree;

/*
  95. Unique Binary Search Trees II
  Given an integer n, generate all structurally unique BST's (binary search trees) that store values 1...n.
 */

import java.util.ArrayList;
import java.util.List;

public class Quiz095 {
    /**
     * 思路：递归
     * 给定一个序列，一次选择其中的各个元素作为根节点，构造BST。左子树由该节点的左边序列构造，递归构造。
     * 右子树同理。最后构造的结果是左右子树个数的乘积。
     */
    public List<TreeNode> generateTrees(int n) {
        List<TreeNode> ans = new ArrayList<>();
        if(n<=0){
            return ans;
        }
        return generateTreesHelper(1, n);
    }

    public List<TreeNode> generateTreesHelper(int start, int end) {
        List<TreeNode> ans = new ArrayList<>();
        if (start > end) {
            ans.add(null);
            return ans;
        }

        for (int i = start; i <= end; i++) {
            List<TreeNode> leftTrees = generateTreesHelper(start, i - 1);
            List<TreeNode> rightTrees = generateTreesHelper(i + 1, end);
            for (TreeNode leftTree : leftTrees) {
                for (TreeNode rightTree : rightTrees) {
                    TreeNode root = new TreeNode(i);
                    root.left = leftTree;
                    root.right = rightTree;
                    ans.add(root);
                }
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        Quiz095 quiz095 = new Quiz095();
        quiz095.generateTrees(0);
    }
}
