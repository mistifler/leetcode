package tree;

/*
116. Populating Next Right Pointers in Each Node

Given a binary tree

struct TreeLinkNode {
  TreeLinkNode *left;
  TreeLinkNode *right;
  TreeLinkNode *next;
}

Populate each next pointer to point to its next right node.
If there is no next right node, the next pointer should be set to NULL.
Initially, all next pointers are set to NULL.

Note:

You may only use constant extra space.
You may assume that it is a perfect binary tree (ie, all leaves are at the same level, and every parent has two children).
For example,
Given the following perfect binary tree,
         1
       /  \
      2    3
     / \  / \
    4  5  6  7
After calling your function, the tree should look like:
         1 -> NULL
       /  \
      2 -> 3 -> NULL
     / \  / \
    4->5->6->7 -> NULL

 */

public class Quiz116 {

    public void connect(TreeLinkNode root) {
        //用于遍历
        TreeLinkNode cur = root;


        while (cur != null) {
            //通过prev建立连接
            TreeLinkNode prev = null;
            //记录下一层开始的位置
            TreeLinkNode nextLevelHead = null;

            for (; cur != null; cur = cur.next) {
                if (nextLevelHead == null) {
                    nextLevelHead = cur.left != null ? cur.left : cur.right;
                }
                if (cur.left != null) {
                    if(prev != null) {
                        prev.next = cur.left;
                    }
                    prev = cur.left;

                }
                if(cur.right != null) {
                    if(prev != null) {
                        prev.next = cur.right;
                    }
                    prev = cur.right;
                }
            }
            cur = nextLevelHead;
        }

    }
}
