package tree;

/*
  111. Minimum Depth of Binary Tree
  Given a binary tree, find its minimum depth.

  The minimum depth is the number of nodes along the shortest path from the root node down to the nearest leaf node.
 */

public class Quiz111 {
    /**
     * 思路1 ：递归
     * 依然是对求树的高度函数的改造。或者说是DFS吧
     * 要特殊考虑一个节只有一个子节点的情况，只能取有叶子节点那个高度。
     * 因为取Min(left, right)的话如果一个为null，就会取到这个小的，而他并不是叶子节点会得到错误的结果
     */
    public int minDepth(TreeNode root) {
        if(root == null) {
            return 0;
        }
        if(root.left == null) {
            return minDepth(root.right) + 1;
        }
        if(root.right == null) {
            return minDepth(root.left) + 1;
        }

        return Math.min(minDepth(root.left), minDepth(root.right)) + 1;
    }
}
