package tree;

/*
  100. Same Tree
  Given two binary trees, write a function to check if they are the same or not.

  Two binary trees are considered the same if they are structurally identical and the nodes have the same value.
 */

public class Quiz100 {
    /**
     * 思路1：递归
     * 不管哪种遍历方式都可以，同时遍历两棵树即可
     */
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q == null) {
            return true;
        }
        if (p == null || q == null) {
            return false;
        }

        return p.val == q.val && isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
    }
}
