package tree;

/*
226. Invert Binary Tree
Invert a binary tree.
             4
           /   \
          2     7
         / \   / \
        1   3 6   9
to
             4
           /   \
          7     2
         / \   / \
        9   6 3   1
 */

public class Quiz226 {
    /**
     * 思路：递归，先序遍历
     */
    public TreeNode invertTree(TreeNode root) {
        if(root == null) return null;
        if(root.left == null && root.right == null) {
            return root;
        }
        TreeNode tmp = root.left;
        root.left = invertTree(root.right);
        root.right = invertTree(tmp);
        return root;
    }
}
