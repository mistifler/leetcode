package tree;

/*
  101. Symmetric Tree
  Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).

  For example, this binary tree [1,2,2,3,4,4,3] is symmetric:
       1
      / \
     2   2
    / \ / \
   3  4 4  3
 */

public class Quiz101 {
    /**
     * 思路：和quiz100的same tree 类似，只不过每次是用自己的左边和自己的右边比较
     */
    public boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }
        return isSymmetricTree(root.left, root.right);
    }

    public boolean isSymmetricTree(TreeNode left, TreeNode right) {
        if (left == null && right == null) {
            return true;
        }
        if (left == null || right == null) {
            return false;
        }

        return left.val == right.val && isSymmetricTree(left.right, right.left) && isSymmetricTree(left.left, right.right);
    }
}
