package tree;

/*
  103. Binary Tree Zigzag Level Order Traversal
  Given a binary tree, return the zigzag level order traversal of its nodes' values. (ie, from left to right, then right to left for the next level and alternate between).

  For example:
  Given binary tree [3,9,20,null,null,15,7],
        3
       / \
      9  20
        /  \
       15   7
  return its zigzag level order traversal as:
    [
      [3],
      [20,9],
      [15,7]
    ]
 */

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Quiz103 {
    /**
     * 思路：层序遍历的一个小变形
     */
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> ans = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        boolean reverseFlag = false;
        if (root == null) {
            return ans;
        }
        queue.offer(root);
        while (!queue.isEmpty()) {
            int levelNum = queue.size();
            List<Integer> levelRet = new ArrayList<>();
            for (int i = 0; i < levelNum; ++i) {
                TreeNode curNode = queue.poll();
                if (curNode.left != null) {
                    queue.offer(curNode.left);
                }
                if (curNode.right != null) {
                    queue.offer(curNode.right);
                }

                if(reverseFlag) {
                    levelRet.add(0, curNode.val);
                }else{
                    levelRet.add(curNode.val);
                }
            }
            ans.add(levelRet);
            reverseFlag = !reverseFlag;
        }
        return ans;
    }
}
