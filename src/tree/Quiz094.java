package tree;

/*
 * 94. Binary Tree Inorder Traversal
 * https://leetcode.com/problems/binary-tree-inorder-traversal/description/

 Given a binary tree, return the inorder traversal of its nodes' values.
 For example:
 Given binary tree [1,null,2,3],
 1
  \
   2
  /
 3
 return [1,3,2].
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Quiz094 {

    /**
     * 思路1 递归
     * 结束条件是当前节点为null。
     * 递归访问左子树，访问当前根节点，递归访问右子树
     */
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> ans = new ArrayList<>();
        inorderTraversalHelper(ans, root);
        return ans;
    }

    public void inorderTraversalHelper(List<Integer> ans, TreeNode root) {
        if(null == root) {
            return;
        }
        inorderTraversalHelper(ans, root.left);
        ans.add(root.val);
        inorderTraversalHelper(ans, root.right);
    }

    /**
     * 思路2：使用栈
     * cur指针遍历，如果cur!=null || !stack.isEmpty,
     * 如果cur!=null stack放入cur，cur = cur.left
     * 否则，取出栈顶访问，cur = cur.right
     */
    public List<Integer> inorderTraversal2(TreeNode root) {
        List<Integer> ans = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        TreeNode cur = root;
        while(cur != null || !stack.isEmpty()) {
            if(cur != null) {
                stack.push(cur);
                cur = cur.left;
            } else {
                TreeNode tmp = stack.pop();
                ans.add(tmp.val);
                cur = tmp.right;
            }
        }
        return ans;
    }
}
