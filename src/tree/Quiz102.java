package tree;

/*
  102. Binary Tree Level Order Traversal
  Given a binary tree, return the level order traversal of its nodes' values. (ie, from left to right, level by level).

  For example:
  Given binary tree [3,9,20,null,null,15,7],
      3
     / \
    9  20
    /  \
   15   7
  return its level order traversal as:
    [
      [3],
      [9,20],
      [15,7]
    ]
 */

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Quiz102 {
    /**
     * 思路：使用一个辅助队列
     */
    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> ans = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();

        if (root == null) {
            return ans;
        }

        queue.offer(root);
        while (!queue.isEmpty()) {
            List<Integer> levelRet = new ArrayList<>();
            int levelNum = queue.size();
            for (int i = 0; i < levelNum; i++) {
                TreeNode curNode = queue.poll();
                if (curNode.left != null) {
                    queue.offer(curNode.left);
                }
                if (curNode.right != null) {
                    queue.offer(curNode.right);
                }
                levelRet.add(curNode.val);
            }
            ans.add(levelRet);
        }
        return ans;
    }
}
