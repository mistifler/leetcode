package tree;

/*
  96. Unique Binary Search Trees
  Given n, how many structurally unique BST's (binary search trees) that store values 1...n?

  For example,
  Given n = 3, there are a total of 5 unique BST's.
 */

public class Quiz096 {
    public int numTrees(int n) {
        //如果n=0 的话，那么下面的f[1]=1就错了所以要写成n+1
        int[] dp = new int[n+1];
        dp[0] = 1;
        dp[1] = 1;

        for(int i=2; i<=n; ++i) {
            for(int j=1; j<=i; ++j) {
                dp[i] += dp[j-1] * dp[i-j];
            }
        }
        return dp[n];
    }

    public static void main(String[] args) {
        Quiz096 quiz096 = new Quiz096();
        System.out.println(quiz096.numTrees(3));
    }
}
