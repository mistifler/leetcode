package tree;

/*
145. Binary Tree Postorder Traversal
Given a binary tree, return the postorder traversal of its nodes' values.

For example:
Given binary tree {1,#,2,3},
   1
    \
     2
    /
   3
return [3,2,1].

Note: Recursive solution is trivial, could you do it iteratively?

 */

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Quiz145 {
    public List<Integer> postorderTraversal(TreeNode root) {
        Stack<TreeNode> stack = new Stack<>();
        List<Integer> list = new ArrayList<>();
        TreeNode pre = null;
        TreeNode current = root;
        while(current != null || !stack.isEmpty()) {
            while(current != null) {
                stack.push(current);
                current = current.left;
            }
            current = stack.pop();
            if(current.right != null && pre != current.right) {
                stack.push(current);
                current = current.right;
                continue;
            }
            list.add(current.val);
            pre = current;
            current = null;

        }
        return list;
    }
}
