package tree;

/*
  107. Binary Tree Level Order Traversal II
  Given a binary tree, return the bottom-up level order traversal of its nodes' values. (ie, from left to right, level by level from leaf to root).

  For example:
  Given binary tree [3,9,20,null,null,15,7],
        3
       / \
      9  20
        /  \
       15   7
  return its bottom-up level order traversal as:
    [
      [15,7],
      [9,20],
      [3]
    ]
 */

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Quiz107 {
    /**
     * 思路：同leetcode102 Binary Tree Level Order Traversal
     */
    public List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> ans = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        if(root == null) {
            return ans;
        }
        queue.offer(root);
        while (!queue.isEmpty()) {
            int levelNum = queue.size();
            List<Integer> levelRet = new ArrayList<>();
            for(int i=0; i<levelNum; ++i) {
                TreeNode curNode = queue.poll();
                if(curNode.left != null) {
                    queue.offer(curNode.left);
                }
                if(curNode.right != null) {
                    queue.offer(curNode.right);
                }
                levelRet.add(curNode.val);
            }
            ans.add(0, levelRet);
        }
        return ans;
    }
}
