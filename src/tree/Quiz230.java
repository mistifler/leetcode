package tree;
/*
230. Kth Smallest Element in a BST
Given a binary search tree, write a function kthSmallest to find the kth smallest element in it.

Note:
You may assume k is always valid, 1 ≤ k ≤ BST's total elements.

Follow up:
What if the BST is modified (insert/delete operations) often and you need to find the kth smallest frequently?
How would you optimize the kthSmallest routine?
 */

public class Quiz230 {
    /**
     * 常见的思路是先序遍历，这里尝试一个比较有新意的思路是二分查找：
     * 计算左子树节点个数为 cnt
     * 如果k <= cnt ，递归遍历左子树找第K个
     * 如果k > cnt + 1，递归遍历右子树，找第 k - 1 - cnt个，这个 1 表示当前根节点
     * 剩下的就是当前root了
     *
     */
    public int kthSmallest(TreeNode root, int k) {
        int leftNodeCnt = countNode(root.left);
        if(k <= leftNodeCnt) {
            return kthSmallest(root.left, k);
        } else if(k > leftNodeCnt + 1) {
            return kthSmallest(root.right, k - 1 - leftNodeCnt);
        }
        return root.val;
    }

    public int countNode(TreeNode root) {
        if(root == null) {
            return 0;
        }
        return 1 + countNode(root.left) + countNode(root.right);
    }
}
