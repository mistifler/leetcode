package tree;

/*
  98. Validate Binary Search Tree
  Given a binary tree, determine if it is a valid binary search tree (BST).

  Assume a BST is defined as follows:

  The left subtree of a node contains only nodes with keys less than the node's key.
  The right subtree of a node contains only nodes with keys greater than the node's key.
  Both the left and right subtrees must also be binary search trees.
 */

import java.util.Stack;

public class Quiz098 {
    /**
     * 思路1，递归
     * 结束条件：如果节点为空，return true
     * 保证当前root 小于左右值，并且左子树和右子树都是合法的BST
     * 可以假设最左的节点不会小于INT_MIN 最右的节点不会大于INT_MAX
     */
    public boolean isValidBST(TreeNode root) {
        return isValidBSTHelper(root, Long.MIN_VALUE, Long.MAX_VALUE);
    }

    public boolean isValidBSTHelper(TreeNode root, long lo, long hi) {
        if (root == null) {
            return true;
        }

        return root.val < hi && root.val > lo &&
                isValidBSTHelper(root.left, lo, (long) root.val) &&
                isValidBSTHelper(root.right, (long) root.val, hi);
    }

    /**
     * 思路2：中序遍历的结果是递增的
     * 只需要用一个变量保存便利顺序的前一个结果即可
     */
    public boolean isValidBST2(TreeNode root) {
        long prev = Long.MIN_VALUE;
        Stack<TreeNode> stack = new Stack<>();
        TreeNode cur = root;
        while(cur != null || !stack.isEmpty()) {
            if(cur != null) {
                stack.push(cur);
                cur = cur.left;
            } else {
                TreeNode tmp = stack.pop();
                if(tmp.val <= prev) {
                    return false;
                }
                cur = tmp.right;
                prev = tmp.val;
            }
        }
        return true;
    }

}
