package tree;

/*
  105. Construct Binary Tree from Preorder and Inorder Traversal
  Given preorder and inorder traversal of a tree, construct the binary tree.

  Note:
  You may assume that duplicates do not exist in the tree.
 */

public class Quiz105 {

    /**
     * 思路：递归构造
     * 递归函数的参数是 先序和中序的迭代器范围
     * 递归函数结束的条件是：迭代起始大于结束
     * 过程：每次取先序的root，并将其从中序中分割，将分割后的子序列递归构造左右子树
     */
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        return buildTreeHelper(0, 0, inorder.length - 1, preorder, inorder);
    }

    public TreeNode buildTreeHelper(int preStart, int inStart, int inEnd, int[] preorder, int[] inorder) {
        if(inStart > inEnd || preStart > preorder.length - 1) {
            return null;
        }

        TreeNode root = new TreeNode(preorder[preStart]);
        int index = 0;
        for(int i=inStart; i<=inEnd; ++i) {
            if(inorder[i] == preorder[preStart]) {
                index = i;
                break;
            }
        }
        root.left = buildTreeHelper(preStart + 1, inStart, index - 1, preorder, inorder);
        root.right = buildTreeHelper(preStart + index - inStart + 1, index + 1, inEnd, preorder, inorder);
        return root;
    }
}
