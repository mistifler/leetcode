package tree;

/*
  99. Recover Binary Search Tree
  Two elements of a binary search tree (BST) are swapped by mistake.

  Recover the tree without changing its structure.

  Note:
  A solution using O(n) space is pretty straight forward. Could you devise a constant space solution?
 */

public class Quiz099 {

    /**
     * 总体思路：O(n)空间的就是用栈进行中序遍历，常数空间的话可以考虑Morris遍历，或者递归调用。
     * 递归的思路：全局保存一个fisrt，second记录错误的节点，prev记录上一个节点。修改递归调用的访问部分的代码，每次判断是否出现了错误节点。
     */
    TreeNode firstNode = null;
    TreeNode secondNode = null;
    TreeNode prevNode = new TreeNode(Integer.MIN_VALUE);

    public void recoverTree(TreeNode root) {
        inorderTraversal(root);
        int tmp = firstNode.val;
        firstNode.val = secondNode.val;
        secondNode.val =tmp;

    }

    public void inorderTraversal(TreeNode root) {
        if(root == null) {
            return;
        }
        inorderTraversal(root.left);
        if(firstNode == null && prevNode.val >= root.val) {
            firstNode = prevNode;
        }
        if(firstNode != null && prevNode.val >= root.val) {
            secondNode = root;
        }
        prevNode = root;
        inorderTraversal(root.right);
    }
}
