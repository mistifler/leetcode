package tree;

/*
124. Binary Tree Maximum Path Sum
Given a binary tree, find the maximum path sum.

For this problem, a path is defined as any sequence of nodes from some starting node to any node in the tree
along the parent-child connections. The path must contain at least one node and does not need to go through the root.

For example:
Given the below binary tree,

       1
      / \
     2   3
Return 6.
 */

public class Quiz124 {
    /**
     * 思路，最大字段和的变形
     * 数组可以从左往右遍历，二叉树的话可以考虑深度优先遍历。每次极端左右子树的值，如果大于0表示对整体有益。用一个全局变量保存全局的值。
     */
    private int maxSum = Integer.MIN_VALUE;
    public int maxPathSum(TreeNode root) {
        maxPathSumDfs(root);
        return maxSum;
    }

    public int maxPathSumDfs(TreeNode root) {
        if(root == null) {
            return 0;
        }

        int leftSum = maxPathSumDfs(root.left);
        int rightSum = maxPathSumDfs(root.right);
        int curSum = root.val;
        if(leftSum > 0){
            curSum += leftSum;
        }
        if(rightSum > 0) {
            curSum += rightSum;
        }
        //更新全局结果
        maxSum = Math.max(curSum, maxSum);
        //同一条路径左右子树只能二选一
        return Math.max(leftSum, rightSum) > 0 ? Math.max(leftSum, rightSum) + root.val : root.val;
    }

}
