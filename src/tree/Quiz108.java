package tree;

/*
  108. Convert Sorted Array to Binary Search Tree
  Given an array where elements are sorted in ascending order, convert it to a height balanced BST.

  For this problem, a height-balanced binary tree is defined as a binary tree in which
  the depth of the two subtrees of every node never differ by more than 1.

    Example:

    Given the sorted array: [-10,-3,0,5,9],

    One possible answer is: [0,-3,9,-10,null,5], which represents the following height balanced BST:

          0
         / \
       -3   9
       /   /
     -10  5
 */

public class Quiz108 {
    /**
     * 思路：每次取中间的元素作为根节点，递归构造左右子树
     * 和二分查找操作区间过程十分相似，[start, end), 结束条件是 start >= end
     */
    public TreeNode sortedArrayToBST(int[] nums) {
        return buildBSTHelper(nums, 0, nums.length - 1);
    }

    public TreeNode buildBSTHelper(int[] nums, int start, int end) {
        if(start >= end) {
            return null;
        }
        int mid = start + (end - start) / 2;
        TreeNode root = new TreeNode(nums[mid]);
        root.left = buildBSTHelper(nums, start, mid);
        root.right = buildBSTHelper(nums, mid + 1, end);
        return root;
    }
}
