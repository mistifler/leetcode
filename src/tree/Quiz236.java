package tree;

/*
236. Lowest Common Ancestor of a Binary Tree
Given a binary tree, find the lowest common ancestor (LCA) of two given nodes in the tree.

According to the definition of LCA on Wikipedia:
“The lowest common ancestor is defined between two nodes v and w as the lowest node in T
that has both v and w as descendants (where we allow a node to be a descendant of itself).”

        _______3______
       /              \
    ___5__          ___1__
   /      \        /      \
   6      _2       0       8
         /  \
         7   4
For example, the lowest common ancestor (LCA) of nodes 5 and 1 is 3. Another example is LCA of nodes 5 and 4 is 5,
since a node can be a descendant of itself according to the LCA definition.
 */

public class Quiz236 {
    /**
     * 思路1：递归
     * 函数DFS，找到p或者q或者dfs结束，都返回。
     * 递归搜索左右两颗树得到结果left 和 right
     * 如果两个都不为空，就表示root是当前的公共祖先。否则就返回不为空的那个
     */
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if(root == null || root == p || root == q) {
            return root;
        }
        TreeNode left = lowestCommonAncestor(root.left, p, q);
        TreeNode right = lowestCommonAncestor(root.right, p, q);
        if(left != null && right != null) {
            return root;
        }
        if(left != null) {
            return left;
        } else {
            return right;
        }
    }

    /**
     * 思路2：dfs保存两个节点的路径，然后找公共的最长路径的最后一个节点
     */
}
