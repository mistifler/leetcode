package tree;

/*
  110. Balanced Binary Tree
  Given a binary tree, determine if it is height-balanced.

  For this problem, a height-balanced binary tree is defined as a binary tree in which the depth of the two subtrees
  of every node never differ by more than 1.


 */

public class Quiz110 {
    /**
     * 思路：改造常见的求树的高度函数，或者说是DFS吧
     * 每次递归结束不再是直接返回树的高度(max(lh, rh) + 1)，而是先看是否当前处于平衡，即 abs(lh - rh) > 1，
     * 如果不平衡直接 return -1 ，表示可以剪枝了
     * 如果平衡的话，再返回树的高度
     */
    public boolean isBalanced(TreeNode root) {
        return dfsHeight(root) != -1;
    }

    public int dfsHeight(TreeNode root) {
        if(root == null) {
            return 0;
        }

        int leftHeight = dfsHeight(root.left);
        if (leftHeight == -1) {
            return -1;
        }
        int rightHeight = dfsHeight(root.right);
        if(rightHeight == -1) {
            return -1;
        }

        if(Math.abs(leftHeight - rightHeight) > 1) {
            return -1;
        }

        return Math.max(leftHeight, rightHeight) + 1;
    }
}
