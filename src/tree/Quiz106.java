package tree;

/*
  106. Construct Binary Tree from Inorder and Postorder Traversal
  Given inorder and postorder traversal of a tree, construct the binary tree.

  Note:
  You may assume that duplicates do not exist in the tree.


 */

public class Quiz106 {

    /**
     * 思路同 leetcode 105 只是要注意post是从后往前看
     */
    public TreeNode buildTree(int[] inorder, int[] postorder) {
        return buildTreeHelper(postorder.length - 1, 0, inorder.length - 1, inorder, postorder);
    }

    public TreeNode buildTreeHelper(int postStart, int inStart, int inEnd, int[] inorder, int[] postorder) {
        if(inStart > inEnd || postStart < 0) {
            return null;
        }

        TreeNode root = new TreeNode(postorder[postStart]);
        int index = 0;
        for(int i=0; i<inorder.length; ++i) {
            if(inorder[i] == postorder[postStart]) {
                index = i;
                break;
            }
        }

        root.left = buildTreeHelper(postStart - (inEnd - index + 1), inStart, index - 1, inorder, postorder);
        root.right = buildTreeHelper(postStart - 1, index + 1, inEnd, inorder, postorder);
        return root;
    }
}
