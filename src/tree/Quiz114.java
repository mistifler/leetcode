package tree;

/*
  114. Flatten Binary Tree to Linked List
  Given a binary tree, flatten it to a linked list in-place.

  For example,
  Given

         1
        / \
       2   5
      / \   \
     3   4   6
  The flattened tree should look like:
   1
    \
     2
      \
       3
        \
         4
          \
           5
            \
             6
 */

import java.util.Stack;

public class Quiz114 {
    /**
     * 思路：先序遍历，借助栈
     * 左节点置为空，遍历的都往右节点上放
     */
    public void flatten(TreeNode root) {
        TreeNode curNode = root;
        Stack<TreeNode> stack = new Stack<>();
        if(curNode != null) {
            stack.push(curNode);
        }
        while(!stack.isEmpty()) {
            curNode = stack.pop();
            if(curNode.right != null) {
                stack.push(curNode.right);
            }
            if(curNode.left != null) {
                stack.push(curNode.left);
            }

            curNode.left = null;
            if(!stack.isEmpty()) {
                curNode.right = stack.peek();
            }
        }

    }

    /**
     * 思路2：递归
     * 先处理左边，再处理右边，最后将结果拼接成链表
     */
    public void flatten2(TreeNode root) {
        if(root == null) {
            return;
        }
        flatten(root.left);
        flatten(root.right);
        if(root.left == null) {
            return;
        }
        TreeNode p = root.left;
        while (p.right != null) {
            p = p.right;
        }
        p.right = root.right;
        root.right = root.left;
        root.left = null;
    }
}
