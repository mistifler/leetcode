package dp;

/*
53. Maximum Subarray
Find the contiguous subarray within an array (containing at least one number) which has the largest sum.

For example, given the array [-2,1,-3,4,-1,2,1,-5,4],
the contiguous subarray [4,-1,2,1] has the largest sum = 6.
 */

public class Quiz053 {
    /**
     * 思路：DP
     * 用 DP[i] 表示数组A[i]结尾的最大子段和，那么最终的目标是找到最大的DP[i]
     * 转移方程：每个数字都有加入和不加入两种操作 DP[i] = Max{DP[i-1] + A[i], A[i]}
     * 只需要看前一个的值，所以没有必要开一个数组。
     * 最终要找最大的DP[i]，可以在遍历过程中直接维护结果即可
     */
    public int maxSubArray(int[] nums) {
        int ans = Integer.MIN_VALUE;
        int dp = 0;
        for(int i=0; i<nums.length; i++) {
            dp = Math.max(dp + nums[i], nums[i]);
            ans = Math.max(ans, dp);
        }
        return ans;
    }

    public static void main(String[] args) {
        Quiz053 quiz053 = new Quiz053();
        int[] test = {-2,1,-3,4,-1,2,1,-5,4};
        int ans = quiz053.maxSubArray(test);
        System.out.println(ans);
    }
}
