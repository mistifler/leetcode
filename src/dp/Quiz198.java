package dp;

/**
 * 198. House Robber
 * You are a professional robber planning to rob houses along a street.
 * Each house has a certain amount of money stashed, the only constraint stopping you from robbing each of them is that
 * adjacent houses have security system connected and it will automatically contact the police
 * if two adjacent houses were broken into on the same night.
 * <p>
 * Given a list of non-negative integers representing the amount of money of each house,
 * determine the maximum amount of money you can rob tonight without alerting the police.
 */

public class Quiz198 {
    /**
     * 思路：动态规划 dp数组
     * dp(n) 表示数组nums[0:n-1]这n个数得到的最大值
     * dp(i) = max{dp(i-2) + nums[i-1] , dp(i-1)}
     */
    public int rob(int[] nums) {
        if (nums.length <= 0) {
            return 0;
        }
        int[] dp = new int[nums.length + 1];
        dp[0] = 0;
        dp[1] = nums[0];
        for (int i = 2; i < nums.length + 1; i++) {
            dp[i] = Math.max(dp[i - 2] + nums[i - 1], dp[i - 1]);
        }
        return dp[nums.length];
    }

    /**
     * 思路2：对思路1进行优化，因为每次 dp(i) 只用到 dp(i-1) dp(i-2) 可以用变量保存，省去了dp数组的存储空间
     * dp(i) = max{dp(i-1), dp(i-2) + nums[i-1]}
     */
    public int rob2(int[] nums) {
        if(nums.length <= 0) {
            return 0;
        }
        //dp(i-1)
        int dp1 = nums[0];
        //dp(i-2)
        int dp2 = 0;
        for (int i = 2; i <= nums.length; ++i) {
            int tmp = Math.max(dp1, dp2 + nums[i-1]);
            dp2 = dp1;
            dp1 = tmp;

        }
        return dp1;
    }

    public static void main(String[] args) {
        Quiz198 quiz198 = new Quiz198();
        int[] nums = {1, 2, 3, 5};
        System.out.println(quiz198.rob2(nums));
    }
}
