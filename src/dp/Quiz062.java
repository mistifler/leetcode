package dp;

/*
62. Unique Paths
A robot is located at the top-left corner of a m x n grid (marked 'Start' in the diagram below).

The robot can only move either down or right at any point in time.
The robot is trying to reach the bottom-right corner of the grid (marked 'Finish' in the diagram below).

How many possible unique paths are there?
Note: m and n will be at most 100.
 */

import java.util.Arrays;

public class Quiz062 {
    /**
     * 思路：DP
     * 备忘录的方法 DP[i][j] 表示走到这里可能的路径个数
     * 转移方程：DP[i][j] = DP[i-1][j] + DP[i][j-1] 表示从上面往下迈一步，或者从左往右迈一步.
     * 在此基础上，可以进一步用滚动数组的思路将DP降低到一维
     */
    public int uniquePaths(int m, int n) {
        int[] dp = new int[n];
        dp[0] = 1;
        for(int i=0; i<m; i++) {
            for (int j=1; j<n; j++) {
                dp[j] += dp[j-1];
            }
        }
        return dp[n-1];
    }

    /**
     * 不用滚动数组的版本
     */
    public int uniquePaths2(int m, int n) {
        int[][] dp = new int[m][n];
        for(int i=0; i<m; i++) dp[i][0] = 1;
        for(int j=0; j<n; j++) dp[0][j] = 1;
        for(int i = 1; i<m; i++) {
            for(int j=1; j<n; j++) {
                dp[i][j] = dp[i-1][j] +dp[i][j-1];
            }
        }
        return dp[m-1][n-1];
    }
}
