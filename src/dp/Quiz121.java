package dp;
/*
121. Best Time to Buy and Sell Stock
Say you have an array for which the ith element is the price of a given stock on day i.

If you were only permitted to complete at most one transaction (ie, buy one and sell one share of the stock), design an algorithm to find the maximum profit.

Example 1:
Input: [7, 1, 5, 3, 6, 4]
Output: 5

max. difference = 6-1 = 5 (not 7-1 = 6, as selling price needs to be larger than buying price)
Example 2:
Input: [7, 6, 4, 3, 1]
Output: 0

In this case, no transaction is done, i.e. max profit = 0.

 */

public class Quiz121 {
    /**
     * 思路：
     * 需要找到最大的差值，可以维护两个变量，一个记录当前遇到的最小的股价curMin，一个记录当前可以取得的最大收益maxProfit
     * 从前往后遍历，不断更新这两个值即可
     */
    public int maxProfit(int[] prices) {
        if(prices.length == 0) {
            return 0;
        }
        int curMin = prices[0];
        int ans = 0;
        for(int i=1; i<prices.length; i++) {
            curMin = Math.min(curMin, prices[i]);
            ans = Math.max(ans, prices[i] - curMin);
        }
        return ans;
    }

    public static void main(String[] args){
        int[] test = {7, 1, 5, 3, 6, 4};
        int[] test2 = {7, 6, 4, 3, 1};
        Quiz121 quiz121 = new Quiz121();
        System.out.println(quiz121.maxProfit(test));
        System.out.println(quiz121.maxProfit(test2));
    }
}
