package dp;
/*
714. Best Time to Buy and Sell Stock with Transaction Fee
Your are given an array of integers prices, for which the i-th element is the price of a given stock on day i; and a non-negative integer fee representing a transaction fee.

You may complete as many transactions as you like, but you need to pay the transaction fee for each transaction. You may not buy more than 1 share of a stock at a time (ie. you must sell the stock share before you buy again.)

Return the maximum profit you can make.

Example 1:
Input: prices = [1, 3, 2, 8, 4, 9], fee = 2
Output: 8
Explanation: The maximum profit can be achieved by:
Buying at prices[0] = 1
Selling at prices[3] = 8
Buying at prices[4] = 4
Selling at prices[5] = 9
The total profit is ((8 - 1) - 2) + ((9 - 4) - 2) = 8.
Note:

0 < prices.length <= 50000.
0 < prices[i] < 50000.
0 <= fee < 50000.
 */

public class Quiz714 {
    /**
     * 思路：动态规划
     * 类似LeetCode 188 ，不过是不限次数，更改了计算最大收益的方式，卖出的时候需要额外减去一个固定的费用
     * 1 先考虑有哪些状态
     * 依然是只有持有和非持有两个状态
     * hold[i] unhold[i]
     *
     * 2 考虑状态转移方程
     * 对于hold[i]
     * hold[i] = max{ hold[i-1], unhold[i-1] - prices[i]}
     *
     * 对于unhold[i]
     * unhold[i] = max{ unhold[i-1], hold[i-1] + prices[i] - fee }
     */
    public int maxProfit(int[] prices, int fee) {
        int len = prices.length;
        if(len < 2) {
            return 0;
        }

        int[] hold = new int[len];
        int[] unhold = new int[len];

        hold[0] = -prices[0];
        unhold[0] = 0;
        for(int i=1; i<len; i++) {
            hold[i] = Math.max(hold[i-1], unhold[i-1] - prices[i]);
            unhold[i] = Math.max(unhold[i-1], hold[i-1] + prices[i] - fee);
        }
        return unhold[len - 1];
    }

    /**
     * 思路2：滚动数组优化
     * 可以从另一个角度来理解：unhold就是我们的账户余额，hold是我们拥有股票的时候的最大收益
     */
    public int maxProfit2(int[] prices, int fee) {
        int len = prices.length;
        if(len < 2) {
            return 0;
        }

        int hold = -prices[0];
        int unhold = 0;
        for(int i=1; i<len; i++) {
            int tmpUnHold = unhold;
            unhold = Math.max(unhold, hold + prices[i] - fee);
            hold = Math.max(hold, tmpUnHold - prices[i]);

        }
        return unhold;
    }

    public static void main(String[] args) {
        int[] test = {1,2,3,4,1,8};
        int fee = 2;
        Quiz714 quiz714 = new Quiz714();
        quiz714.maxProfit(test, 0);
    }
}
