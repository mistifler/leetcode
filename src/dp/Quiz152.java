package dp;
/*
152. Maximum Product Subarray
Find the contiguous subarray within an array (containing at least one number) which has the largest product.

For example, given the array [2,3,-2,4],
the contiguous subarray [2,3] has the largest product = 6.
 */

public class Quiz152 {
    /**
     * 思路：动态规划
     * 最大字段和的变形，负负得正，所以每次既要保存当前最大的值，也要保存当前最小的值，然后根据当前这个数是否大于零来不断更新
     */
    public int maxProduct(int[] nums) {
        if(nums.length == 0) {
            return 0;
        }
        int ans = nums[0];
        int curMax = nums[0];
        int curMin = nums[0];

        for(int i=1; i<nums.length; ++i) {
            if(nums[i] < 0) {
                //swap(curMax, curMin)
                int tmp = curMax;
                curMax = curMin;
                curMin = tmp;
            }

            //当前元素加入序列or当前元素自己另起一个连续的序列
            curMax = Math.max(nums[i], nums[i] * curMax);
            curMin = Math.min(nums[i], nums[i] * curMin);

            ans = Math.max(curMax, ans);
        }
        return ans;
    }

    public static void main(String[] args) {
        int[] test = {2,0,9};
        Quiz152 quiz152 = new Quiz152();
        System.out.println(quiz152.maxProduct(test));
    }
}
