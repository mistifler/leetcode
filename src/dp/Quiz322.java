package dp;

import java.util.*;

/**
 * 322. Coin Change
 * link: https://leetcode.com/problems/coin-change/description/
 * <p>
 * You are given coins of different denominations and a total amount of money amount.
 * Write a function to compute the fewest number of coins that you need to make up that amount.
 * If that amount of money cannot be made up by any combination of the coins, return -1.
 * <p>
 * Example 1:
 * coins = [1, 2, 5], amount = 11
 * return 3 (11 = 5 + 5 + 1)
 * <p>
 * Example 2:
 * coins = [2], amount = 3
 * return -1.
 * <p>
 * Note:
 * You may assume that you have an infinite number of each kind of coin.
 */
public class Quiz322 {

    /**
     * 思路：DP
     * 用数组存储过去的结果，因为amount最大的值是全部为1(如果有)，所以可以初始化为amount + 1
     * 递推关系：dp[i] = min{dp[i-coin] + 1} coin表示所有可能的面值。
     * 最后结果 dp[amount] > amount ? -1 : dp.[amount]
     */
    public int coinChange(int[] coins, int amount) {
        List<Integer> dp = new ArrayList<>(Collections.nCopies(amount + 1, amount + 1));
        dp.set(0, 0);
        for(int i=1; i<=amount; i++) {
            for(int coin: coins) {
                if(coin <= i) {
                    dp.set(i, Math.min(dp.get(i), dp.get(i-coin) + 1));
                }
            }
        }

        return dp.get(amount) > amount ? -1 : dp.get(amount);
    }


    /**
     * 思路2：map优化
     * 第一次认为硬币面值是任意的。用了map来存储之前的结果，如果不能找零则不会存在于map中。
     * dp[i] = min{dp[i-coin] + 1} coin表示所有可能的面值。
     */
    //：以下认为硬币面值可以是任意的，用map来存储，可以节省一定的空间，但代码不够简洁
    public int coinChange2(int[] coins, int amount) {
        if(coins.length <= 0) {
            return -1;
        }
        if(amount <= 0) {
            return 0;
        }

        Map<Integer, Integer> dp = new HashMap<>();
        for (int coin : coins) {
            dp.put(coin, 1);
        }
        for (int cur = 1; cur <= amount; cur++) {
            if (dp.containsKey(cur)) {
                continue;
            }
            int minVal = Integer.MAX_VALUE;
            for (int coin : coins) {
                if (dp.containsKey(cur - coin)) {
                    minVal = Math.min(minVal, dp.get(cur -coin) + 1);
                }
            }
            if(minVal != Integer.MAX_VALUE){
                dp.put(cur, minVal);
            }

        }
        return dp.getOrDefault(amount, -1);
    }

    public static void main(String[] args) {
        Quiz322 test = new Quiz322();
        int[] coins1 = {1,2,5};
        int amount = 11;
        System.out.println(test.coinChange(coins1, amount));
    }
}
