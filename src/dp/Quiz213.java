package dp;

/**
 * 213. House Robber II
 * Note: This is an extension of House Robber.
 * <p>
 * After robbing those houses on that street, the thief has found himself a new place for his thievery
 * so that he will not get too much attention. This time, all houses at this place are arranged in a circle.
 * That means the first house is the neighbor of the last one.
 * Meanwhile, the security system for these houses remain the same as for those in the previous street.
 * <p>
 * Given a list of non-negative integers representing the amount of money of each house,
 * determine the maximum amount of money you can rob tonight without alerting the police.
 */


public class Quiz213 {
    /**
     * 思路：转换成原问题，将首尾去掉，得到两个子数组，分别求解。
     */
    public int rob(int[] nums) {
        //小坑，只有一个元素的情况
        if(nums.length == 1) {
            return nums[0];
        }
        return Math.max(robHelper(nums, 0, nums.length - 2), robHelper(nums, 1, nums.length - 1));
    }

    /**
     * 给定数组并制定初始下标和结束下标，计算houserobber的最大收益
     */
    public int robHelper(int[] nums, int lo, int hi) {
        int length = hi - lo + 1;
        if (length < 1) {
            return 0;
        }

        int dp2 = 0;
        int dp1 = nums[lo];
        for (int i = 1; i < length; i++) {
            int tmp = Math.max(dp1, dp2 + nums[lo + i]);
            dp2 = dp1;
            dp1 = tmp;
        }
        return dp1;
    }

    public static void main(String[] args) {
        Quiz213 quiz213 = new Quiz213();
        int[] nums = {1, 2, 3, 5, 7};
        System.out.println(quiz213.rob(nums));
    }

}
