package dp;
/*
188. Best Time to Buy and Sell Stock IV
Say you have an array for which the ith element is the price of a given stock on day i.

Design an algorithm to find the maximum profit. You may complete at most k transactions.

Note:
You may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).
 */

public class Quiz188 {
    /**
     * 思路：动态规划
     *
     * 1 首先搞清楚k次交易的概念，即买了k次，如果考虑出手的话只是将第k次交易执行完，但是不能k+1
     *
     * 2 然后是要考虑有哪些状态，k次，i天，然后第i天还可能有两种状态，即第一天持有股票或者没有持有股票，所以归纳下来建立两个方程
     * hold[k][i] unhold[k][i] 分别表示在前i天，最多进行了k次交易并且在持有股票或这没有持有股票的情况下的最大收益
     *
     * 3 接着就要考虑状态转移方程了
     *
     * 3.1 先看hold的，第i天持有股票有两种可能情况
     * (1) 第i天刚买即进行了一次交易，unhold[k-1][i] - prices[i]
     * (2) 第i天不买即第i-1天前已经是持有状态了，hold[k][i-1]
     * 所以
     * hold[k][i] = max{ unhold[k-1][i] - prices[i] , hold[k][i-1] }
     *
     * 3.2 再看unhold，第i天不持有股票同意由两种可能情况
     * (1) 第i天刚好出手，hold[k][i-1] + prices[i]
     * (2) 之前已经出手过，unhold[k][i-1]
     * 所以
     * unhold[k][i] = max{ hold[k][i-1] + prices[i], unhold[k][i-1] }
     *
     * 4 其他细节，k如果给的很大或者k已经超过数组长度的一半了
     * 这时的情况就退化成买卖股票2了，即相当于可以进行任意多次，如果动态规划会超时
     *
     * 参考：https://segmentfault.com/a/1190000006672807
     */
    public int maxProfit(int k, int[] prices) {
        int len = prices.length;
        if (len <= 1) {
            return 0;
        }
        if (k >= len / 2) {
            return quickSolve(prices);
        }

        int[][] hold = new int[k + 1][len];
        int[][] unhold = new int[k + 1][len];

        for (int i = 1; i <= k; i++) {
            //最多进行i次交易收益情况初始化
            hold[i][0] = -prices[0];
            unhold[i][0] = 0;
            for (int j = 1; j < len; j++) {
                //第j天持有股票：第j天买入，或者之前已经买入过
                hold[i][j] = Math.max(unhold[i - 1][j] - prices[j], hold[i][j - 1]);

                //第j天不持有股票：第j天卖出，或者之前已经卖出了
                unhold[i][j] = Math.max(hold[i][j - 1] + prices[j], unhold[i][j - 1]);
            }
        }

        //最后无论如何都要出手，才能取得最大收益
        return unhold[k][len - 1];
    }


    /**
     * 思路2：动态规划 来自讨论区
     * 设DP[k][i] 表示不超过k次交易，0-i天可以获得的最大收益
     * 动态转移方程 DP[k][i] = max{ dp[k][i-1], prices[i] + tmpMax }
     * 即第i天不超过k次的最大收益是 第i-1天已经进行了k次交易，第i天前进行了一次交易并且过去所花费的钱不超过k次情况下的最小花费
     * tmpMax初始状态为 -prices[0]，随着i的迭代，tmpMax = max{tmpMax, dp[k-1][i-1] - prices[i]}
     * tmpMax的意义为，在第i天进行了买入，并且在0~i-1天最多执行了k-1次交易可以取得的最大收益，这个是被用到下次循环
     */
    public int maxProfit2(int k, int[] prices) {
        int len = prices.length;
        if (k >= len / 2) {
            return quickSolve(prices);
        }
        int[][] dp = new int[k + 1][len];
        for (int i = 1; i <= k; i++) {
            int tmpMax = -prices[0];
            for (int j = 1; j < len; j++) {
                dp[i][j] = Math.max(dp[i][j - 1], prices[j] + tmpMax);
                tmpMax = Math.max(tmpMax, dp[i - 1][j - 1] - prices[j]);
            }
        }
        return dp[k][len - 1];
    }

    private int quickSolve(int[] prices) {
        int len = prices.length;
        int profit = 0;
        for (int i = 1; i < len; i++) {
            if (prices[i] > prices[i - 1]) {
                profit += prices[i] - prices[i - 1];
            }
        }
        return profit;
    }

    public static void main(String[] args) {
        int[] test = {6, 1, 3, 2, 4, 7};
    }
}
