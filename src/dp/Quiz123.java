package dp;
/*
123. Best Time to Buy and Sell Stock III
Say you have an array for which the ith element is the price of a given stock on day i.

Design an algorithm to find the maximum profit. You may complete at most two transactions.

Note:
You may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).
 */

public class Quiz123 {
    /**
     * 思路：动态规划
     * 和之前相比，现在最多只允许进行两次交易，而且必须是先卖出才能买进。
     * 举个例子 {1,7,3,9} 当买第一支股票的时候，1买入，肯定在9的时候卖出收益最好，但是最多可以进行两次交易，也许在中间某次再进行一次交易可以使得总体收益更大
     * 既然如此我们可以考虑在 位置 i 的前后进行了两次交易，问题转化成一个双向动态规划，
     * 我们用 DPLeft[i] 表示在位置 i 之前进行了一次交易所获取的最大收益，用DPRight[right] 表示位置 i 之后一次交易获取的最大收益
     * 那么最终的整体最大收益就是 Max{DPLeft[i] + DPRight[i] | i = 0...n}
     *
     * more: leetcode188 扩展到不定k次，需要二维动态规划了
     */
    public int maxProfit(int[] prices) {
        if(prices.length < 2) {
            return 0;
        }

        int ans = 0;

        int[] dpl = new int[prices.length];
        int[] dpr = new int[prices.length];

        int curMin = prices[0];
        for(int i=1; i<prices.length; i++) {
            curMin = Math.min(prices[i], curMin);
            dpl[i] = Math.max(dpl[i-1], prices[i] - curMin);
        }

        int curMax = prices[prices.length - 1];
        for(int i=prices.length-2; i>=0; i--) {
            curMax = Math.max(prices[i], curMax);
            dpr[i] = Math.max(dpr[i+1], curMax - prices[i]);
        }

        for(int i=0; i<prices.length; i++) {
            ans = Math.max(ans, dpl[i] + dpr[i]);
        }
        return ans;
    }

    public static void main(String[] args) {
        int[] stocks = {1,7,3,9};
        Quiz123 quiz123 = new Quiz123();
        System.out.println(quiz123.maxProfit(stocks));
    }
}
