package dp;
/*
309. Best Time to Buy and Sell Stock with Cooldown
Say you have an array for which the ith element is the price of a given stock on day i.

Design an algorithm to find the maximum profit.
You may complete as many transactions as you like (ie, buy one and sell one share of the stock multiple times) with the following restrictions:

You may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).
After you sell your stock, you cannot buy stock on next day. (ie, cooldown 1 day)
Example:

prices = [1, 2, 3, 0, 2]
maxProfit = 3
transactions = [buy, sell, cooldown, buy, sell]
 */

public class Quiz309 {
    /**
     * 思路：动态规划
     *
     * 和LeetCode188类似，不过是不限次数，但是状态增加了
     * 1 先思考有哪些状态，这里很显然在第 i 天，可能持有股票，不持有股票，或者处于冷却期(cooldown),可以用三个数组代表：
     * hold[i], unhold[i], cooldown[i]
     *
     * 2 考虑三种状态的状态转移方程
     * 持有状态 hold[i]
     * 持有状态只能由unhold状态转移过来同时在第i天买入股票，或者保持不变，所以
     * hold[i] = max{ hold[i-1], unhold[i-1] - prices[i] }
     *
     * 非持有状态 unhold[i]
     * 非持有状态可能是由 cooldown 状态转移过来，或者保持不变，所以
     * unhold[i] = max{ unhold[i-1], cooldown[i-1] }
     *
     * 冷却状态 cooldown[i]
     * 冷却状态只有一种可能，即出售了股票
     * cooldown[i] = hold[i-1] + prices[i]
     *
     */
    public int maxProfit(int[] prices) {
        int len = prices.length;
        if(len <= 1) {
            return 0;
        }

        int[] hold = new int[len];
        int[] unhold = new int[len];
        int[] cooldown = new int[len];

        hold[0] = -prices[0];
        unhold[0] = 0;
        cooldown[0] = Integer.MIN_VALUE;

        for(int i=1; i<len; i++) {
            hold[i] = Math.max(hold[i-1], unhold[i-1] - prices[i]);
            unhold[i] = Math.max(cooldown[i-1], unhold[i-1]);
            cooldown[i] = hold[i-1] + prices[i];
        }

        return Math.max(cooldown[len-1], unhold[len-1]);
    }

    /**
     * 思路2：滚动数组优化
     * 既然所有的状态都只用到了前一个，所以可以用滚动数组优化，将空间复杂度降低到常数
     * 不过这个时候就要注意状态更改的顺序，保证被读的状态不能在本次循环先被赋值了。
     *
     * 也可以从另一个角度来理解：unhold 和 cooldown 都可以认为是我们现在的现金或者说账户余额，hold是我们在持有股票时候的最大收益
     */
    public int maxProfit2(int[] prices) {
        int len = prices.length;
        if(len <= 1) {
            return 0;
        }

        int hold = -prices[0];
        int unhold = 0;
        int cooldown = Integer.MIN_VALUE;

        for(int i=1; i<len; i++) {
            int preCooldown = cooldown;
            cooldown = hold + prices[i];
            hold = Math.max(hold, unhold - prices[i]);
            unhold = Math.max(preCooldown, unhold);

        }

        return Math.max(cooldown, unhold);
    }
}
