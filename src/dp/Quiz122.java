package dp;
/*
122. Best Time to Buy and Sell Stock II
Say you have an array for which the ith element is the price of a given stock on day i.

Design an algorithm to find the maximum profit. You may complete as many transactions as you like
(ie, buy one and sell one share of the stock multiple times). However,
you may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).
 */

public class Quiz122 {
    /**
     * 思路：贪心
     * 和之前的相比，可以交易的次数不限，但是在买入之前必须先卖出。
     * 总体的获利就是连续的峰值-谷底，比如 1 5 3 6 4，获利情况就是 5-1 + 6-3 = 7
     * 我们不必关注不连续的更高的峰值，比如我们不需要考虑1的时候买入，6的时候卖出，这样的收益一定不会比之前的收益高。可以通过作图简单看出来
     *
     * 所以现在的策略是一旦发现后一天的价格会升高，就今天买入明天卖出获取收益即可
     *
     */
    public int maxProfit(int[] prices) {
        int ans = 0;
        for(int i=1; i<prices.length; ++i) {
            if(prices[i] > prices[i-1]) {
                ans += prices[i] - prices[i-1];
            }
        }
        return ans;
    }

    /**
     * 思路2：动态规划
     * 首先确定下有哪些状态：在第i天可能是持有或者非持有股票的状态，抽象出两个数组
     * hold[i] unhold[i] 表示在第i天持有或者没有持有股票能取得的最大收益
     *
     * 然后确定状态转移方程
     * hold[i] = Math.max{ hold[i-1], unhold[i-1] - prices[i] }
     * unhold[i] = Math.max{ unhold[i-1], hold[i-1] + prices[i] }
     *
     * 进一步的可以用滚动数组优化
     *
     */
    public int maxProfit2(int[] prices) {
        int len = prices.length;
        if(len < 2) {
            return 0;
        }

        int hold = -prices[0];
        int unhold = 0;
        for(int i=1; i<len; i++) {
            int tmpHold = hold;
            hold = Math.max(hold, unhold - prices[i]);
            unhold = Math.max(unhold, tmpHold + prices[i]);
        }

        return unhold;
    }
}
