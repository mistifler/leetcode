package dp;

/*
55. Jump Game
Given an array of non-negative integers, you are initially positioned at the first index of the array.

Each element in the array represents your maximum jump length at that position.

Determine if you are able to reach the last index.

For example:
A = [2,3,1,1,4], return true.

A = [3,2,1,0,4], return false.
 */

public class Quiz055 {
    /**
     * 思路1：贪心
     * 每次看能跳到的最远距离，一旦发现可以超过最后一个位置，就返回true，否则返回false
     * 遍历时候i的上限是当前的最远距离
     */
    public boolean canJump(int[] nums) {
        int farPos = 0;
        for (int i = 0; i <= farPos; ++i) {
            farPos = Math.max(farPos, i + nums[i]);
            if (farPos >= nums.length - 1) {
                return true;
            }
        }
        return false;
    }

    /**
     * 思路2：动态规划
     * 可以用 DP[i]表示跳到第 i 个位置的时候还剩多少步可以继续往前走,那么如果 DP[n-1] >=0 就表示可以跳到最后一个位置
     * DP[i] = Math.max(DP[i-1], A[i-1]) - 1 表示跳到第i个位置的时候或许是之前的一次大跳，或许是从当前位置跳过去
     */
    public boolean canJump2(int[] nums) {
        int dp = 0;
        for (int i = 1; i < nums.length; ++i) {
            dp = Math.max(dp, nums[i - 1]) - 1;
            if (dp < 0) {
                return false;
            }
        }
        return true;
    }
}
