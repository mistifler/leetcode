package dp;

/**
 * 10. Regular Expression Matching
 * <p>
 * Implement regular expression matching with support for '.' and '*'.
 * <p>
 * '.' Matches any single character.
 * '*' Matches zero or more of the preceding element.
 * <p>
 * The matching should cover the entire input string (not partial).
 * <p>
 * The function prototype should be:
 * bool isMatch(const char *s, const char *p)
 * <p>
 * Some examples:
 * isMatch("aa","a") → false
 * isMatch("aa","aa") → true
 * isMatch("aaa","aa") → false
 * isMatch("aa", "a*") → true
 * isMatch("aa", ".*") → true
 * isMatch("ab", ".*") → true
 * isMatch("aab", "c*a*b") → true
 */


public class Quiz010 {
    /**
     * 思路1：递归
     * <p>
     * 主要是对星号 * 所代表的0个或多个前缀字符的处理，如果遇到了星号：
     * isMath(text, pattern.substring(2)) || 当前字符匹配 && isMatch(text.substring(1), pattern)
     * <p>
     * 例如 aab a* 当前字符匹配并递归 isMatch("ab", "a*") ，*还可以表示0个，所以直接后移pattern isMatch(aab, "")。二者是逻辑或的关系
     * 从这个例子也可以看出递归结束的条件：pattern.isEmpty()同时 text.isEmpty() 表示完全匹配上了。
     * <p>
     * 小坑：逻辑或的两个关系顺序不能颠倒，否则会出现StringIndexOutOfBoundsException，比如 isMatch("", ".*")
     */
    public boolean isMatch(String s, String p) {
        if (p.isEmpty()) {
            return s.isEmpty();
        }

        boolean curMatch = !s.isEmpty() && (s.charAt(0) == p.charAt(0) || p.charAt(0) == '.');
        if (p.length() >= 2 && p.charAt(1) == '*') {
            return isMatch(s, p.substring(2)) || (curMatch && isMatch(s.substring(1), p));
        } else {
            return curMatch && isMatch(s.substring(1), p.substring(1));
        }
    }

    /**
     * 思路2：动态规划 自底向上
     * <p>
     * 题目满足最优子结构特点，可以考虑备忘录法保存递归的各个isMatch的结果
     * DP(i, j) 表示 isMatch(s.substring(i), p.substring(j))的结果，自底向上需要先从DP(s.length, p.length)递推到DP(0,0)，相当于递归的结束部分，
     * 状态转移方程可以按照递归的逻辑直接改写
     */
    public boolean isMatch2(String s, String p) {
        boolean[][] dp = new boolean[s.length() + 1][p.length() + 1];
        dp[s.length()][p.length()] = true;
        for (int i = s.length(); i >= 0; --i) {
            for (int j = p.length() - 1; j >= 0; --j) {
                boolean curMatch = (i < s.length()) && (s.charAt(i) == p.charAt(j) || p.charAt(j) == '.');
                if (j + 1 < p.length() && p.charAt(j + 1) == '*') {
                    dp[i][j] = dp[i][j + 2] || curMatch && dp[i + 1][j];
                } else {
                    dp[i][j] = curMatch && dp[i + 1][j + 1];
                }
            }
        }
        return dp[0][0];
    }

    /**
     * 思路3：动态规划 自顶向下(备忘录？)
     * 通过备忘录保存递归的结果
     */
    public boolean isMatch3(String s, String p) {
        memo = new Result[s.length() + 1][p.length() + 1];
        return dp(0, 0, s, p);
    }

    enum Result {
        TRUE, FALSE
    }

    Result[][] memo;

    public boolean dp(int i, int j, String s, String p) {
        if (memo[i][j] != null) {
            return memo[i][j] == Result.TRUE;
        }
        boolean ans;
        if (j == p.length()) {
            return i == s.length();
        } else {
            boolean curMatch = (i < s.length()) && (s.charAt(i) == p.charAt(j) || p.charAt(j) == '.');
            if (j + 1 < p.length() && p.charAt(j + 1) == '*') {
                ans = dp(i, j + 2, s, p) || curMatch && dp(i + 1, j, s, p);
            } else {
                ans = curMatch && dp(i + 1, j + 1, s, p);
            }
        }
        //缓存结果
        memo[i][j] = ans ? Result.TRUE : Result.FALSE;
        return ans;
    }


    public static void main(String[] args) {
        Quiz010 quiz010 = new Quiz010();
//        System.out.println(quiz010.isMatch("aa", "a"));
//        System.out.println(quiz010.isMatch("aa", "aa"));
//        System.out.println(quiz010.isMatch("aaa", "aa"));
//        System.out.println(quiz010.isMatch("aa", "a*"));
//        System.out.println(quiz010.isMatch("aa", ".*"));
//        System.out.println(quiz010.isMatch("ab", ".*"));
//        System.out.println(quiz010.isMatch("aab", "c*a*b"));

        System.out.println(quiz010.isMatch2("ab", ".*c"));
        System.out.println(quiz010.isMatch3("ab", ".*c"));
    }
}
