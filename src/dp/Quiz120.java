package dp;
/*
120. Triangle
Given a triangle, find the minimum path sum from top to bottom. Each step you may move to adjacent numbers on the row below.

For example, given the following triangle
[
     [2],
    [3,4],
   [6,5,7],
  [4,1,8,3]
]
The minimum path sum from top to bottom is 11 (i.e., 2 + 3 + 5 + 1 = 11).

Note:
Bonus point if you are able to do this using only O(n) extra space, where n is the total number of rows in the triangle.
 */

import java.util.List;

public class Quiz120 {
    /**
     * 思路：动态规划
     * 从最结尾开始向上反推
     * DP(i,j) 表示从最下层走到(i,j)的最小值，DP(0,0) 就是最小的路径
     * 转移方程 DP(i, j) = min{DP(i+1,j), DP(i+1, j+1)} + array(i, j)
     * 要求只能是 O(n)的空间复杂度，所以需要用滚动数组优化，或者说是降维操作，将高维去掉，外层依然是i去做循环，内层 dp(j) = min(dp(j), dp(j+1)) + array(i,j)
     */
    public int minimumTotal(List<List<Integer>> triangle) {
        int rowCnt = triangle.size();
        int[] dp = new int[rowCnt];
        //初始化DP数组为triangle的最后一层
        for(int i=0; i<triangle.get(rowCnt - 1).size(); i++) {
            dp[i] = triangle.get(rowCnt-1).get(i);
        }
        for(int layer=triangle.size()-2; layer>=0; layer--){
            for(int i=0; i<=layer; i++){
                dp[i] = Math.min(dp[i], dp[i+1]) + triangle.get(layer).get(i);
            }
        }
        return dp[0];
    }
}
