package dp;

/*
63. Unique Paths II
Follow up for "Unique Paths":

Now consider if some obstacles are added to the grids. How many unique paths would there be?

An obstacle and empty space is marked as 1 and 0 respectively in the grid.

For example,
There is one obstacle in the middle of a 3x3 grid as illustrated below.

[
  [0,0,0],
  [0,1,0],
  [0,0,0]
]
The total number of unique paths is 2.

Note: m and n will be at most 100.

 */

public class Quiz063 {
    /**
     * 思路：DP
     * DP[i][j] = DP[i-1][j] + DP[i][j-1]，在此基础上，如果该格子为1，DP[i][j] = 0
     * 如果可以改变给的数组，就不需要另外开DP数组，这里还是另外开一个
     */
    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        int m = obstacleGrid.length;
        int n = obstacleGrid[0].length;

        int[][] dp = new int[m][n];
        dp[0][0] = obstacleGrid[0][0] ^ 1;

        //初始化边界
        for(int i=1; i<m; ++i) {
            dp[i][0] = obstacleGrid[i][0] == 0 ? dp[i-1][0] : 0;
        }
        for(int j=1; j<n; ++j) {
            dp[0][j] = obstacleGrid[0][j] == 0 ? dp[0][j-1] : 0;
        }
        //状态转移
        for(int i=1; i<m; ++i) {
            for(int j=1; j<n; ++j) {
                dp[i][j] = obstacleGrid[i][j] == 0 ? (dp[i-1][j] + dp[i][j-1]) : 0;
            }
        }
        return dp[m-1][n-1];
    }

    /**
     * 思路：备忘录法
     */
    public int uniquePathsWithObstacles3(int[][] obstacleGrid) {
        int m = obstacleGrid.length;
        int n = obstacleGrid[0].length;

        return 0;
    }

    public static void main(String[] args) {
        int[][] test = new int[2][2];
        System.out.println(test[0][1]);
    }
}
