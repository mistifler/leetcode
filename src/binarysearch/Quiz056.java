package binarysearch;

/*
56. Merge Intervals
Given a collection of intervals, merge all overlapping intervals.

For example,
Given [1,3],[2,6],[8,10],[15,18],
return [1,6],[8,10],[15,18].
 */

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Quiz056 {
    public List<Interval> merge(List<Interval> intervals) {
        List<Interval> ans = new ArrayList<>();
        for(int i=0; i<intervals.size(); ++i) {
            insert(ans, intervals.get(i));
        }
        return ans;
    }

    public List<Interval> insert(List<Interval> intervals, Interval newInterval) {
        int startLb = listLowerBound(intervals.stream().map(x->x.start).collect(Collectors.toList()), newInterval.start);
        int endLb = listLowerBound(intervals.stream().map(x->x.end).collect(Collectors.toList()), newInterval.end);
        if(startLb > 0 && intervals.get(startLb - 1).end >= newInterval.start) {
            startLb--;
            newInterval.start = intervals.get(startLb).start;
        }

        if(endLb < intervals.size() && intervals.get(endLb).start <= newInterval.end) {
            newInterval.end = intervals.get(endLb).end;
            endLb++;
        }

        intervals.subList(startLb, endLb).clear();
        intervals.add(startLb, newInterval);

        return intervals;
    }

    /**
     * 给定一个整数和一个整数List，返回lowerBound
     */
    public int listLowerBound(List<Integer> nums, int target) {
        int left = 0;
        int right = nums.size();
        while (left < right) {
            int mid = left + (right - left) / 2;
            if(nums.get(mid) < target) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return left;
    }
}
