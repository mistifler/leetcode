package binarysearch;

/*
33. Search in Rotated Sorted Array

Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

(i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).

You are given a target value to search. If found in the array return its index, otherwise return -1.

You may assume no duplicate exists in the array.


 */

public class Quiz033 {
    /**
     * 思路：二分搜索
     * 旋转后可以认为是两个递增序列，并且第一个序列所有的值都大于第二个，比如例子中的 [4,5,6,7] [0,1,2]
     * 每次用left 和 mid 比较，如果 left <= mid 表示mid处于第一个序列，也即[left, mid) 区间有序，否则[mid, right) 有序。
     * 利用有序的特性，比较target是否处于有序的序列中，移动左右指针
     * 细节上两个小tips：
     * (1) 使用[1] [1,2]等例子检验自己的代码是否会数组越界
     * (2) 检查代码里是否有不安全的行为，比如left mid right-1属于安全的访问，left+1 mid+1不安全
     */
    public int search(int[] nums, int target) {
        int left = 0;
        int right = nums.length;
        while (left < right) {
            int mid = (right + left) >> 1;
            if (nums[mid] == target) {
                return mid;
            }
            //[left, mid) 有序
            if (nums[left] <= nums[mid]) {
                if (nums[left] <= target && nums[mid] > target) {
                    //考察[left, mid) 区间
                    right = mid;
                } else {
                    //考察[mid+1, right) 区间
                    left = mid + 1;
                }
            } else {
                //[mid+1, right) 有序
                if (nums[mid] < target && nums[right - 1] >= target) {
                    //考察[mid+1, right) 区间
                    left = mid + 1;
                } else {
                    right = mid;
                }
            }
        }
        return -1;
    }
}
