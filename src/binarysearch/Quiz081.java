package binarysearch;
/*
81. Search in Rotated Sorted Array II
Follow up for "leetcode33 Search in Rotated Sorted Array":
What if duplicates are allowed?

Would this affect the run-time complexity? How and why?
Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

(i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).

Write a function to determine if a given target is in the array.

The array may contain duplicates.

 */

public class Quiz081 {
    /**
     * 思路：二分搜索
     * 和之前不同的是多了重复的元素，所以之前如果 nums[left] <= nums[mid]可以判断这个区间有序就不行了，只能是nums[left] < nums[mid]才可以确定
     * 那现在就是要单独处理相等的情况比如可能是 [1,3,1,1]，[1,1,1,1] 这时候相当于退化成O(n)了，单独移动left++，来继续判断
     */
    public boolean search(int[] nums, int target) {
        int left = 0;
        int right = nums.length;
        while (left < right) {
            int mid = left + (right - left) / 2;
            if(nums[mid] == target) {
                return true;
            }
            if(nums[left] < nums[mid]) {
                if(nums[left] <= target && nums[mid] > target) {
                    right = mid;
                } else {
                    left = mid + 1;
                }
            } else if(nums[left] > nums[mid]) {
                if(nums[mid] < target && nums[right - 1] >= target) {
                    left = mid + 1;
                } else {
                    right = mid;
                }
            } else {
                left++;
            }
        }
        return false;
    }
}
