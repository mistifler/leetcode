package binarysearch;

/*
34. Search for a Range
Given an array of integers sorted in ascending order, find the starting and ending position of a given target value.

Your algorithm's runtime complexity must be in the order of O(log n).

If the target is not found in the array, return [-1, -1].

For example,
Given [5, 7, 7, 8, 8, 10] and target value 8,
return [3, 4].
 */

public class Quiz034 {
    /**
     * 思路：自己实现二分版本的lowerBound和upperBound
     * 整理下 mid 和 target 的关系有两组：mid < target mid >= target 和 mid <= target mid > target
     * mid < target, 移动left = mid+1，target区间仍在[left,right)里，left可以最终移动到 lowerBound
     * mid <= target, 移动left = mid+1，会导致left移动到区间里最终刚好移动到target区间外，求得upperBound
     */
    public int[] searchRange(int[] nums, int target) {
        int[] ans = {-1, -1};
        int lo = lowerBound(nums, target);
        int hi = upperBound(nums, target);
        if (lo < hi) {
            ans[0] = lo;
            ans[1] = hi - 1;
            return ans;
        } else {
            return ans;
        }
    }

    public int lowerBound(int[] nums, int target) {
        int left = 0;
        int right = nums.length;
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (nums[mid] < target) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return left;
    }

    public int upperBound(int[] nums, int target) {
        int left = 0;
        int right = nums.length;
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (nums[mid] <= target) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return left;
    }

    public static void main(String[] args) {
        Quiz034 quiz034 = new Quiz034();
        int[] testCase = {5, 7, 7, 8, 10};
        int[] ans = quiz034.searchRange(testCase, 8);
        System.out.println(ans[0] + "," + ans[1]);

//        System.out.println(quiz034.lowerBound(testCase, 4));
//        System.out.println(quiz034.lowerBound(testCase, 6));
//        System.out.println(quiz034.lowerBound(testCase, 7));
//        System.out.println(quiz034.lowerBound(testCase, 12));

        System.out.println(quiz034.upperBound(testCase, 4));
        System.out.println(quiz034.upperBound(testCase, 6));
        System.out.println(quiz034.upperBound(testCase, 7));
        System.out.println(quiz034.upperBound(testCase, 12));
    }
}
