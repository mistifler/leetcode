package binarysearch;

/*
57. Insert Interval
Given a set of non-overlapping intervals, insert a new interval into the intervals (merge if necessary).

You may assume that the intervals were initially sorted according to their start times.

Example 1:
Given intervals [1,3],[6,9], insert and merge [2,5] in as [1,5],[6,9].

Example 2:
Given [1,2],[3,5],[6,7],[8,10],[12,16], insert and merge [4,9] in as [1,2],[3,10],[12,16].

This is because the new interval [4,9] overlaps with [3,5],[6,7],[8,10].
 */

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Quiz057 {
    /**
     * 思路：二分查找
     * 首先在所有的start和end里面找到lower_bound分别为 startLb, endLb，然后进行比较
     * 对于 startLb，需要考察(startLb-1).end是否大于target.start，如果是的话将 startLb 前移，同时让target.start = startLb.start
     * 对于 endLb，需要考察 endLb.start 是否小于等于 target.end，如果是的话将 target.end = endLb.end,然后endLb后移
     * 最后把 [startLb, endLb) 删除，并把 target插入进去
     */
    public List<Interval> insert(List<Interval> intervals, Interval newInterval) {
        int startLb = listLowerBound(intervals.stream().map(x->x.start).collect(Collectors.toList()), newInterval.start);
        int endLb = listLowerBound(intervals.stream().map(x->x.end).collect(Collectors.toList()), newInterval.end);
        if(startLb > 0 && intervals.get(startLb - 1).end >= newInterval.start) {
            startLb--;
            newInterval.start = intervals.get(startLb).start;
        }

        if(endLb < intervals.size() && intervals.get(endLb).start <= newInterval.end) {
            newInterval.end = intervals.get(endLb).end;
            endLb++;
        }

        intervals.subList(startLb, endLb).clear();
        intervals.add(startLb, newInterval);

        return intervals;
    }

    /**
     * 给定一个整数和一个整数List，返回lowerBound
     */
    public int listLowerBound(List<Integer> nums, int target) {
        int left = 0;
        int right = nums.size();
        while (left < right) {
            int mid = left + (right - left) / 2;
            if(nums.get(mid) < target) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return left;
    }

    public static void main(String[] args) {
        Interval interval = new Interval(1,2);
        Interval interval2 = new Interval(3,5);
        Interval interval3 = new Interval(6,7);
        Interval interval4 = new Interval(8,10);
        Interval interval5 = new Interval(12,16);
        List<Interval> test = new ArrayList<>();
        test.add(interval);
        test.add(interval2);
        test.add(interval3);
        test.add(interval4);
        test.add(interval5);

        Interval target = new Interval(4,9);

        Quiz057 quiz057 = new Quiz057();
        System.out.println(quiz057.insert(test, target));
    }
}
