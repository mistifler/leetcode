package binarysearch;
/*
74. Search a 2D Matrix
Write an efficient algorithm that searches for a value in an m x n matrix. This matrix has the following properties:

Integers in each row are sorted from left to right.
The first integer of each row is greater than the last integer of the previous row.
For example,

Consider the following matrix:

[
  [1,   3,  5,  7],
  [10, 11, 16, 20],
  [23, 30, 34, 50]
]
Given target = 3, return true.
 */

public class Quiz074 {
    /**
     * 思路：二分查找
     * 将结束区间定位 m*n 即可
     */
    public boolean searchMatrix(int[][] matrix, int target) {
        if(matrix.length == 0) {
            return false;
        }
        int m = matrix.length;
        int n = matrix[0].length;
        int left = 0;
        int right = m * n;
        while(left < right) {
            int mid = left + (right - left) / 2;
            int val = matrix[mid/n][mid%n];
            if(val == target) {
                return true;
            } else if(val < target) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return false;
    }
}
