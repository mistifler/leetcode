package binarysearch;
/*
153. Find Minimum in Rotated Sorted Array.
Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

(i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).

Find the minimum element.

You may assume no duplicate exists in the array.

 */

public class Quiz153 {
    /**
     * 思路：二分查找
     * 因为不知道中间的值是否就是最小的,所以这里采用 [left, right] 的区间形式
     * 首先确定 [left, right] 中left < right ，如果是的话说明没有rotated，直接返回left即可
     * 否则，说明有反转
     * 接着判断 left <= mid 说明左半边是一个递增的序列，在有反转的情况下，最小值肯定在右半部分，left = mid
     * 否则说明右半部分是一个递增序列，那最小值肯定在左边，right = mid
     *
     * 切割到最后可能出现的情况是 [2,1] 不会出现 [3,2,1]这种连续三个降序的。
     */
    public int findMin(int[] nums) {

        int left = 0;
        int right = nums.length - 1;

        while(left < right) {
            if(nums[left] < nums[right]) {
                return nums[left];
            }

            int mid = left + (right - left) / 2;

            if(nums[left] <= nums[mid]) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }

        return nums[left];
    }

    public static void main(String[] args) {
        Quiz153 quiz153 = new Quiz153();
        int[] nums = {2,1};
        System.out.println(quiz153.findMin(nums));
    }
}
