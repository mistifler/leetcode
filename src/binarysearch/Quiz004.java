package binarysearch;

/*
4. Median of Two Sorted Arrays
There are two sorted arrays nums1 and nums2 of size m and n respectively.

Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).

Example 1:
nums1 = [1, 3]
nums2 = [2]

The median is 2.0
Example 2:
nums1 = [1, 2]
nums2 = [3, 4]

The median is (2 + 3)/2 = 2.5

 */

public class Quiz004 {
    /**
     * 思路：二分
     * 每次比较两个数组的中间位置，可以排除掉其中一个数组一半的元素。
     * 递归结束的条件是其中一个数组已全部排除掉，或者第k个元素为1可以直接求出来
     */
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int total = nums1.length + nums2.length;
        if (total % 2 != 0) {
            return findKth(nums1, 0, nums2, 0, total / 2 + 1);
        } else {
            return (findKth(nums1, 0, nums2, 0, total / 2) + findKth(nums1, 0, nums2, 0, total / 2 + 1)) / 2.0;
        }
    }

    /**
     * ia，ib分别表示nums1和nums2的起始位置，k表示第k大个数
     */
    public double findKth(int[] nums1, int ia, int[] nums2, int ib, int k) {
        int alen = nums1.length - ia;
        int blen = nums2.length - ib;

        //小数组在前，大数组在后
        if (alen > blen) {
            return findKth(nums2, ib, nums1, ia, k);
        }
        //小数组已为空，直接返回大数组的第k个
        if (alen == 0) {
            return nums2[ib + k - 1];
        }
        //k已经等于1，直接返回两个数组较小的那个，两个顺序不能变否则这里可能会数组越界
        if (k == 1) {
            return Math.min(nums1[ia], nums2[ib]);
        }

        //第一个数组查看第m个元素
        int m = Math.min(alen, k / 2);
        //第二个元素查看第 k-m 个元素
        int n = k - m;

        if (nums1[ia + m - 1] < nums2[ib + n - 1]) {
            return findKth(nums1, ia + m, nums2, ib, k - m);
        } else if (nums1[ia + m - 1] > nums2[ib + n - 1]) {
            return findKth(nums1, ia, nums2, ib + n, k - n);
        } else {
            return nums1[ia + m - 1];
        }
    }

    public static void main(String[] args) {
        int[] nums1 = {2, 4};
        int[] nums2 = {1, 3, 5, 6};
        Quiz004 quiz004 = new Quiz004();
        System.out.println(quiz004.findKth(nums1, 0, nums2, 0, 3));
        System.out.println(quiz004.findKth(nums1, 0, nums2, 0, 4));
        System.out.println(quiz004.findMedianSortedArrays(nums1, nums2));
    }
}
