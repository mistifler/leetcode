package binarysearch;
/*
154. Find Minimum in Rotated Sorted Array II
Follow up for "Find Minimum in Rotated Sorted Array":
What if duplicates are allowed?

Would this affect the run-time complexity? How and why?
Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

(i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).

Find the minimum element.

The array may contain duplicates.

 */
public class Quiz154 {
    /**
     * 思路：二分
     * 同LeetCode152，唯一不同的处理是当 left == mid 的时候只能一个个遍历
     */
    public int findMin(int[] nums) {
        int left = 0;
        int right = nums.length - 1;

        while (left < right) {
            if(nums[left] < nums[right]) {
                return nums[left];
            }
            int mid = left + (right - left) / 2;
            if(nums[left] < nums[mid]) {
                left = mid;
            } else if(nums[left] > nums[mid]) {
                right = mid;
            } else {
                left++;
            }
        }

        return nums[left];
    }
}
