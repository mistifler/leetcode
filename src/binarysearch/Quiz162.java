package binarysearch;
/*
162. Find Peak Element
A peak element is an element that is greater than its neighbors.

Given an input array where num[i] ≠ num[i+1], find a peak element and return its index.

The array may contain multiple peaks, in that case return the index to any one of the peaks is fine.

You may imagine that num[-1] = num[n] = -∞.

For example, in array [1, 2, 3, 1], 3 is a peak element and your function should return the index number 2.

click to show spoilers.
 */
public class Quiz162 {
    /**
     * 思路1：遍历
     * 发现拐点即可
     */
    public int findPeakElement(int[] nums) {
        for(int i=0; i<nums.length - 1; i++) {
            if(nums[i] > nums[i+1]){
                return i;
            }
        }
        return nums.length - 1;
    }

    /**
     * 思路2：二分
     * 查看中间位置处，看是否依然保持有序
     */
    public int findPeakElement2(int[] nums) {
        int left = 0;
        int right = nums.length - 1;

        while (left < right) {
            int mid = left + (right - left) / 2;
            if(nums[mid] > nums[mid + 1]) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        return left;
    }
}
