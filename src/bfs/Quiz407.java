package bfs;

import java.util.*;

/**
 * 407. Trapping Rain Water II
 * link: https://leetcode.com/problems/trapping-rain-water-ii/description/
 * <p>
 * Given an m x n matrix of positive integers representing the height of each unit cell in a 2D elevation map,
 * compute the volume of water it is able to trap after raining.
 * <p>
 * Note:
 * Both m and n are less than 110. The height of each unit cell is greater than 0 and is less than 20,000.
 * <p>
 * Example:
 * <p>
 * Given the following 3x6 height map:
 * [
 * [1,4,3,1,3,2],
 * [3,2,1,3,2,4],
 * [2,3,3,2,3,1]
 * ]
 * <p>
 * Return 4.
 */

public class Quiz407 {

    class Gird {
        int x;
        int y;
        int val;

        public Gird(int x, int y, int val) {
            this.x = x;
            this.y = y;
            this.val = val;
        }
    }

    /**
     * 思路：优先权队列
     * 核心思想依然是水是由最小的边界决定的，不同于一维的问题，可以简单的通过两个变量知道自己的左右边界，所以这里考虑用优先权队列去依次从小到大访问边界。
     *
     * 具体实现：
     * 先把所有的边界按照 从小到大 的顺序放入优先权队列，并将其标记为已访问。
     * 取出队列开头的元素，并查看其周围的四个方向，如果没有被访问过，就可以向其中注水(因为可以保证他四个方向的边界都由比他大的)，然后将其标记为已访问。
     * 重复上述步骤直至优先权队列为空
     */
    public int trapRainWater(int[][] heightMap) {
        if (heightMap == null || heightMap.length == 0 || heightMap[0].length == 0) {
            return 0;
        }

        int ans = 0;
        int m = heightMap.length;
        int n = heightMap[0].length;

        //初始化最小堆和visited数组
        boolean[][] visited = new boolean[m][n];
        PriorityQueue<Gird> q = new PriorityQueue<>(new Comparator<Gird>() {
            @Override
            public int compare(Gird o1, Gird o2) {
                return o1.val - o2.val;
            }
        });
        for (int i = 0; i < n; ++i) {
            q.add(new Gird(0, i, heightMap[0][i]));
            q.add(new Gird(m - 1, i, heightMap[m - 1][i]));
            visited[0][i] = true;
            visited[m - 1][i] = true;
        }
        for (int i = 1; i < m; ++i) {
            q.add(new Gird(i, 0, heightMap[i][0]));
            q.add(new Gird(i, n - 1, heightMap[i][n - 1]));
            visited[i][0] = true;
            visited[i][n - 1] = true;
        }

        int[][] dirs = new int[][]{{-1,0}, {1,0}, {0,-1}, {0,1}};
        while (!q.isEmpty()) {
            Gird cur = q.poll();
            //判断上下左右四个方向的邻居格点,如果没有访问过，将其同化，并标记visited数组
            for(int[] dir : dirs) {
                int x = cur.x + dir[0];
                int y = cur.y + dir[1];

                if(x >=0 && x < m && y >=0 && y< n && !visited[x][y]) {
                    visited[x][y] = true;
                    ans += Math.max(0, cur.val - heightMap[x][y]);
                    q.offer(new Gird(x, y, Math.max(cur.val, heightMap[x][y])));
                }
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        Quiz407 quiz407 = new Quiz407();

        //4
        int[][] testCase = {
                {1, 4, 3, 1, 3, 2},
                {3, 2, 1, 3, 2, 4},
                {2, 3, 3, 2, 3, 1},
                {5, 5, 5, 5, 5, 5}
        };

        //0
        int[][] testCase2 = {
                {2,3,4},
                {5,6,7},
                {8,9,10},
                {11,12,13},
                {14,15,16}
        };

        int ret = quiz407.trapRainWater(testCase2);
        System.out.println(ret);

    }
}
