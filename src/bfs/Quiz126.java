package bfs;

/*
Given two words (beginWord and endWord), and a dictionary's word list, find all shortest transformation sequence(s) from beginWord to endWord, such that:

Only one letter can be changed at a time
Each transformed word must exist in the word list. Note that beginWord is not a transformed word.
For example,

Given:
beginWord = "hit"
endWord = "cog"
wordList = ["hot","dot","dog","lot","log","cog"]
Return
  [
    ["hit","hot","dot","dog","cog"],
    ["hit","hot","lot","log","cog"]
  ]
Note:
Return an empty list if there is no such transformation sequence.
All words have the same length.
All words contain only lowercase alphabetic characters.
You may assume no duplicates in the word list.
You may assume beginWord and endWord are non-empty and are not the same.
UPDATE (2017/1/20):
The wordList parameter had been changed to a list of strings (instead of a set of strings). Please reload the code definition to get the latest changes.
 */

import java.util.*;

public class Quiz126 {
    public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
        //层次遍历，如果用queue的话，同一层次会存在重复的问题，一方面会影响效率，一方面在生产路径的时候会有重复
        HashSet<String> cur = new HashSet<>();
        HashSet<String> next = new HashSet<>();
        //判重
        HashSet<String> visited = new HashSet<>();
        //是否找到解，找到可以直接return
        boolean found = false;
        //记录每个状态的父亲节点，用于回溯生成路径
        Map<String, List<String>> fathers = new HashMap<>();

        List<List<String>> ans = new ArrayList<>();

        //将给定的List预处理以下，加速查找效率
        HashSet<String> wordSet = new HashSet<>();
        for(String word : wordList) {
            wordSet.add(word);
        }

        if(!wordSet.contains(endWord)) {
            return ans;
        }

        cur.add(beginWord);
        while (!cur.isEmpty() && !found) {
            //将该层次的节点标记为已访问
            for(String curWord : cur) {
                visited.add(curWord);
            }
            for(String curWord : cur) {
                List<String> newWords = stateExtend(curWord, wordSet, endWord, visited);
                for(String newWord : newWords) {
                    if(isTargetState(newWord, endWord)) {
                        found = true;
                    }
                    next.add(newWord);
                    //记录父节点信息
                    if(fathers.containsKey(newWord)) {
                        fathers.get(newWord).add(curWord);
                    } else {
                        List<String> list = new ArrayList<>();
                        list.add(curWord);
                        fathers.put(newWord, list);
                    }
                }
            }
            cur.clear();
            for(String nextWord : next) {
                cur.add(nextWord);
            }
            next.clear();
        }
        if(found) {
            List<String> path = new ArrayList<>();
            getAllPath(fathers, ans, path, beginWord, endWord);
            return ans;
        }
        return ans;
    }

    boolean isTargetState(String word, String endWord) {
        return word.equals(endWord);
    }

    //状态转移函数，尝试将原单词的每个位置用a-z的字符替换
    List<String> stateExtend(String word, HashSet<String> wordList, String endWord, HashSet<String> visited) {
        List<String> ans = new ArrayList<>();
        char[] wordChars = word.toCharArray();
        for (int i = 0; i < word.length(); i++) {
            char tmp = wordChars[i];
            for (char c = 'a'; c <= 'z'; c++) {
                wordChars[i] = c;
                String str = new String(wordChars);
                //在没有访问过得情况下，如果新的word在字典里或者和endWord相等
                if((wordList.contains(str) || str.equals(endWord)) && !visited.contains(str)) {
                    ans.add(str);
                }
            }
            wordChars[i] = tmp;
        }
        return ans;
    }

    /**
     * 从结尾的endWord回溯到startWord
     */
    public void getAllPath(Map<String, List<String>> fathers, List<List<String>> ans, List<String> path,
                           String start, String end) {
        path.add(end);
        if(end.equals(start)) {
            List<String> ret = new ArrayList<>(path);
            Collections.reverse(ret);
            ans.add(ret);
        } else {
            for(String str : fathers.get(end)) {
                getAllPath(fathers, ans, path, start, str);
            }
        }
        path.remove(path.size() -1);
    }

    public static void main(String[] args) {
        String startWord = "hit";
        String endWord = "cog";
        List<String> wordList = Arrays.asList("hot","dot","dog","lot","log","cog");
        // [["hit","hot","dot","dog","cog"],["hit","hot","lot","log","cog"]]

        Quiz126 quiz126 = new Quiz126();
        System.out.println(quiz126.findLadders(startWord, endWord, wordList).toString());

        String startWord2 = "red";
        String endWord2 = "tax";
        List<String> wordList2 = Arrays.asList("ted","tex","red","tax","tad","den","rex","pee");
//        [["red","ted","tad","tax"],["red","ted","tex","tax"],["red","rex","tex","tax"]]
        System.out.println(quiz126.findLadders(startWord2, endWord2, wordList2).toString());

    }
}
