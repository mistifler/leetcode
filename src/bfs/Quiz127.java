package bfs;

/*
127. Word Ladder
Given two words (beginWord and endWord), and a dictionary's word list, find the length of shortest transformation sequence from beginWord to endWord, such that:

Only one letter can be changed at a time.
Each transformed word must exist in the word list. Note that beginWord is not a transformed word.
For example,

Given:
beginWord = "hit"
endWord = "cog"
wordList = ["hot","dot","dog","lot","log","cog"]
As one shortest transformation is "hit" -> "hot" -> "dot" -> "dog" -> "cog",
return its length 5.

Note:
Return 0 if there is no such transformation sequence.
All words have the same length.
All words contain only lowercase alphabetic characters.
You may assume no duplicates in the word list.
You may assume beginWord and endWord are non-empty and are not the same.
UPDATE (2017/1/20):
The wordList parameter had been changed to a list of strings (instead of a set of strings). Please reload the code definition to get the latest changes.
 */

import java.util.*;

public class Quiz127 {
    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        //路径深度
        int level = 0;
        //层次遍历
        Queue<String> queue = new LinkedList<>();
        //判重
        HashSet<String> visited = new HashSet<>();
        //是否找到解，找到可以直接return
        boolean found = false;

        queue.offer(beginWord);
        visited.add(beginWord);

        HashSet<String> wordListSet = new HashSet<>();
        for(String word : wordList) {
            wordListSet.add(word);
        }

        while (!queue.isEmpty() && !found) {
            level++;
            int levelCnt = queue.size();
            for (int i = 0; i < levelCnt; i++) {
                String curWord = queue.poll();
                visited.add(curWord);
                List<String> newWords = stateExtend(curWord, wordListSet, endWord, visited);
                for (String newWord : newWords) {
                    queue.add(newWord);
                    if (isTargetState(newWord, endWord)) {
                        found = true;
                        break;
                    }
                }
            }
        }

        if (found) {
            return level + 1;
        } else {
            return 0;
        }
    }

    boolean isTargetState(String word, String endWord) {
        return word.equals(endWord);
    }

    //状态转移函数，尝试将原单词的每个位置用a-z的字符替换
    List<String> stateExtend(String word, HashSet<String> wordList, String endWord, HashSet<String> visited) {
        List<String> ans = new ArrayList<>();
        char[] wordChars = word.toCharArray();
        for (int i = 0; i < word.length(); i++) {
            char tmp = wordChars[i];
            for (char c = 'a'; c <= 'z'; c++) {
                wordChars[i] = c;
                String str = new String(wordChars);
                //在没有访问过得情况下，如果新的word在字典里或者和endWord相等
                if (!visited.contains(str) && wordList.contains(str)) {
                    ans.add(str);
                }
            }
            wordChars[i] = tmp;
        }
        return ans;
    }

    public static void main(String[] args) {
        String startWord = "hit";
        String endWord = "cog";
        List<String> wordList = Arrays.asList("hot", "dot", "dog", "lot", "log", "cog");

        Quiz127 quiz127 = new Quiz127();
        int ans = quiz127.ladderLength(startWord, endWord, wordList);
        System.out.println(ans);
    }
}
