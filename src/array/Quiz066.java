package array;

/*
66. Plus One
Given a non-negative integer represented as a non-empty array of digits, plus one to the integer.

You may assume the integer do not contain any leading zero, except the number 0 itself.

The digits are stored such that the most significant digit is at the head of the list.
 */

public class Quiz066 {
    /**
     * 思路：细节实现题
     * 只有为9才会进位，否则直接+1就可以结束了
     * 如果全部是9，就会导致第一位的数最后变成了0，这是重新new 一个全0的数组，然后把第一位置为1即可
     */
    public int[] plusOne(int[] digits) {
        for (int i = digits.length - 1; i >= 0; i--) {
            if(digits[i] != 9) {
                digits[i]++;
                break;
            } else {
                digits[i] = 0;
            }

        }
        if(digits[0] == 0) {
            int[] ans = new int[digits.length + 1];
            ans[0] = 1;
            return ans;
        }
        return digits;
    }
}
