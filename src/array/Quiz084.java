package array;
/*
84. Largest Rectangle in Histogram
Given n non-negative integers representing the histogram's bar height where the width of each bar is 1,
find the area of largest rectangle in the histogram.

Above is a histogram where width of each bar is 1, given height = [2,1,5,6,2,3].
The largest rectangle is shown in the shaded area, which has area = 10 unit.

For example,
Given heights = [2,1,5,6,2,3],
return 10.
 */

import java.util.Stack;

public class Quiz084 {
    /**
     * 思路：栈
     * 栈里面放的单调递增的柱形的索引，可能是不连续的。
     * 比如 i~j 依次递增，当j+1的时候发现递减了，就开始计算面积，直到遇到一个k，使得 i..k, j+1 依然保持高度递增，同时 k+1...j已经计算完面积
     * 计算面积每次向前看，因为后面那个已经是短板
     * 为了保证代码工整，可以考虑最后放个高度为0的柱状图，强制向前更新面积
     */
    public int largestRectangleArea(int[] heights) {
        int ans = 0;
        int len = heights.length;
        Stack<Integer> stack = new Stack<>();

        for (int i = 0; i <= len; i++) {
            //如果遍历到最后，强制开始更新面积
            int h = (i == len ? 0 : heights[i]);

            if (stack.isEmpty() || h >= heights[stack.peek()]) {
                //保证栈中的索引代表的高度是递增的
                stack.push(i);
            } else {
                //开始计算面积
                int tmph = heights[stack.pop()];
                //宽度有两种情况：
                //1如果栈为空，表示宽度可以向前看到最开始，也就是i
                //2 如果栈不为空，只需要看到栈里后面的那个元素的位置即可
                int width = stack.isEmpty() ? i : i - stack.peek() - 1;
                ans = Math.max(ans, tmph * width);
                i--;
            }
        }
        return ans;
    }
}
