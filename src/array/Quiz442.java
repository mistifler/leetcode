package array;

import java.util.ArrayList;
import java.util.List;

/**
 442. Find All Duplicates in an Array
 link: https://leetcode.com/problems/find-all-duplicates-in-an-array/description/

 Given an array of integers, 1 ≤ a[i] ≤ n (n = size of array), some elements appear twice and others appear once.

 Find all the elements that appear twice in this array.

 Could you do it without extra space and in O(n) runtime?

 Example:
 Input:
 [4,3,2,7,8,2,3,1]

 Output:
 [2,3]
 */
public class Quiz442 {
    /**
     * 思路：桶排序
     * 和LeetCode 41 类似，利用数组作为天然的桶，A[i] 存放 i+1
     */
    public List<Integer> findDuplicates(int[] nums) {
        List<Integer> ret = new ArrayList();

        for(int i=0; i<nums.length; ++i) {
            while(nums[i] != i + 1 && nums[nums[i] - 1] != nums[i]) {
                int tmp = nums[i];
                nums[i] = nums[nums[i] - 1];
                nums[tmp - 1] = tmp;
            }
        }

        for(int i=0; i<nums.length; i++) {
            if(nums[i] != i + 1) {
                ret.add(nums[i]);
            }
        }

        return ret;
    }

    public static void main(String[] args) {
        Quiz442 quiz442 = new Quiz442();
        int[] nums = {4,3,2,7,8,2,3,1};
        List<Integer> ret = quiz442.findDuplicates(nums);
        System.out.println(ret.toString());
    }
}
