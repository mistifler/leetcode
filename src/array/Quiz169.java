package array;
/*
169. Majority Element
Given an array of size n, find the majority element. The majority element is the element that appears more than ⌊ n/2 ⌋ times.

You may assume that the array is non-empty and the majority element always exist in the array.
 */

public class Quiz169 {
    /**
     * 思路：投票计数
     * 初始化一个boss，并维护当前boss的投票计数信息
     * 遍历后续元素，相同则计数加一，不同则计数减一，然后计算当前boss的计数信息
     * 一旦发现计数为0，选举当前元素为新的boss，继续后续遍历
     * 因为有元素出现次数超过一半，所以最后boss一定是超过一半的这个数字
     */
    public int majorityElement(int[] nums) {
        int boss = nums[0];
        int curCnt = 1;
        for(int i=1; i<nums.length; i++) {
            if(nums[i] == boss) {
                curCnt++;
            } else {
                curCnt--;
            }

            if(curCnt == 0) {
                boss = nums[i];
                curCnt = 1;
            }
        }
        return boss;
    }
}
