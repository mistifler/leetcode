package array;

import java.util.HashMap;
import java.util.Map;

/*
219. Contains Duplicate II
Given an array of integers and an integer k, find out whether there are two distinct indices i and j in the array
such that nums[i] = nums[j] and the absolute difference between i and j is at most k.
 */

public class Quiz219 {
    /**
     * 思路：HashMap
     * 记录数字和下标信息
     */
    public boolean containsNearbyDuplicate(int[] nums, int k) {
        int len = nums.length;
        if(len < 1) {
            return false;
        }

        Map<Integer, Integer> hashMap = new HashMap<>();
        for(int i=0; i<nums.length; i++) {
            if(hashMap.containsKey(nums[i])) {
                if(Math.abs(i - hashMap.get(nums[i])) <= k) {
                    return true;
                }
            }
            hashMap.put(nums[i], i);
        }
        return false;
    }
}
