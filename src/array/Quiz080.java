package array;
/*
80. Remove Duplicates from Sorted Array II
Follow up for "Remove Duplicates":
What if duplicates are allowed at most twice?

For example,
Given sorted array nums = [1,1,1,2,2,3],

Your function should return length = 5, with the first five elements of nums being 1, 1, 2, 2 and 3.
It doesn't matter what you leave beyond the new length.
 */

public class Quiz080 {
    /**
     * 思路：双指针
     * LeetCode 26 Remove Duplicates from Sorted Array 的变形，即最多允许两次
     * index之前的都表示符合要求，所以需要加一个计数器记录当前index出现了几次，如果超过两次就后移。
     */
    public int removeDuplicates(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        int index = 0;
        int indexCnt = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != nums[index]) {
                index++;
                nums[index] = nums[i];
                indexCnt = 1;
            } else {
                indexCnt++;
                if (indexCnt == 2) {
                    index++;
                    nums[index] = nums[i];
                }
                //如果indexCnt大于2，continue即可，直到找到下一个不相等的地方，index继续向后移动
            }
        }
        return index + 1;
    }
}
