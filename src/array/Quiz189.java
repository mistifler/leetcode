package array;

import java.util.Arrays;

/*
189. Rotate Array
Rotate an array of n elements to the right by k steps.

For example, with n = 7 and k = 3, the array [1,2,3,4,5,6,7] is rotated to [5,6,7,1,2,3,4].

Note:
Try to come up as many solutions as you can, there are at least 3 different ways to solve this problem.

[show hint]

Hint:
Could you do it in-place with O(1) extra space?
Related problem: Reverse Words in a String II
 */

public class Quiz189 {
    /**
     * 思路：
     * 要求不用额外数组，元素组A在转择点处被分割为 A1 A2
     * 分别反转 A1 和 A2，然后将整个数组反转即可
     * 比如：
     * 1 2 3 4 | 5 6 7
     * 4 3 2 1 | 7 6 5
     * 5 6 7 1 2 3 4
     */
    public void rotate(int[] nums, int k) {
        int len = nums.length;
        int r = k % len;
        reverse(nums, 0, len - r - 1);
        reverse(nums, len - r, len - 1);
        reverse(nums, 0, len - 1);
    }

    private void reverse(int[] nums, int left, int right) {
        while (left < right) {
            int tmp = nums[left];
            nums[left] = nums[right];
            nums[right] = tmp;
            left++;
            right--;
        }
    }

    public static void main(String[] args) {
        int[] nums = {1,2,3,4,5,6,7};
        Quiz189 quiz189 = new Quiz189();
        quiz189.rotate(nums, 3);
        System.out.println(nums.toString());
    }
}
