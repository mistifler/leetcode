package array;

/*
73. Set Matrix Zeroes
Given a m x n matrix, if an element is 0, set its entire row and column to 0. Do it in place.

Follow up:
Did you use extra space?
A straight forward solution using O(mn) space is probably a bad idea.
A simple improvement uses O(m + n) space, but still not the best solution.
Could you devise a constant space solution?
 */

public class Quiz073 {
    /**
     * 思路：细节实现
     * 要求常数空间，可以考虑将第一行和第一列用于记录改行或者该列是否需要被全部置为0
     * 1 如果matrix[i][j] == 0 ,就把对应的行首[i][0]和列首[0][j] 都置为0
     * 2 将除了第一行和第一列的遍历一次，如果发现行首或者列首为0，就将其置为0
     * 3 第一行和第一列是否需要置为0可以用两个boolean记录下
     */
    public void setZeroes(int[][] matrix) {
        int m = matrix.length;
        int n = matrix[0].length;
        boolean row = false;
        boolean col = false;
        //标记
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == 0) {
                    matrix[0][j] = 0;
                    matrix[i][0] = 0;
                    if (i == 0) row = true;
                    if (j == 0) col = true;
                }
            }
        }
        //将除了第一行和第一列的其他区域按照标记决定是否置为0
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                if (matrix[i][0] == 0 || matrix[0][j] == 0) {
                    matrix[i][j] = 0;
                }
            }
        }
        //第一行是否需要清0
        if (row) {
            for (int j = 0; j < n; j++) {
                matrix[0][j] = 0;
            }
        }
        //第一列是否需要清0
        if (col) {
            for (int i = 0; i < m; i++) {
                matrix[i][0] = 0;
            }
        }
    }
}
