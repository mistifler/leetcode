package array;

/*
41. First Missing Positive
Given an unsorted integer array, find the first missing positive integer.

For example,
Given [1,2,0] return 3,
and [3,4,-1,1] return 2.

Your algorithm should run in O(n) time and uses constant space.


 */

public class Quiz041 {
    /**
     * 思路：桶排序简化版
     * 需要找正整数，遍历nums数组的每个位置，尝试让 nums[i-1] = i 最后再找出第一个不符合的即可
     * 尝试排序的时候即让 nums[i] 这个值放到其对应的位置
     */
    public int firstMissingPositive(int[] nums) {
        int n = nums.length;
        for (int i = 0; i < n; i++) {
            while (nums[i] > 0 && nums[i] <= n && nums[nums[i] - 1] != nums[i]) {
                //如果先计算nums[i]，num[nums[i] - 1]是有可能出现数组越界的
                int temp = nums[nums[i] - 1];
                nums[nums[i] - 1] = nums[i];
                nums[i] = temp;

            }
        }
        for (int i = 0; i < n; i++) {
            if (nums[i] != i + 1) {
                return i + 1;
            }
        }
        return n + 1;
    }

    public static void main(String[] args) {
        Quiz041 quiz041 = new Quiz041();
        int[] test = {3, 4, -1, 1};
        System.out.println(quiz041.firstMissingPositive(test));
    }
}
