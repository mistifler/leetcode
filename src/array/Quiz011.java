package array;

/*
11. Container With Most Water
Given n non-negative integers a1, a2, ..., an, where each represents a point at coordinate (i, ai).
n vertical lines are drawn such that the two endpoints of line i is at (i, ai) and (i, 0).
Find two lines, which together with x-axis forms a container, such that the container contains the most water.

Note: You may not slant the container and n is at least 2.
 */

public class Quiz011 {
    /**
     * 思路：双指针
     * 水的面积是由最小的木板决定的，维护开头和结尾两个指针，每次移动较小的那个，每次的面积就是较小的那个乘以两个指针的距离
     */
    public int maxArea(int[] height) {
        int left = 0;
        int right = height.length - 1;
        int ans = Integer.MIN_VALUE;

        while (left < right) {
            int curArea = Math.min(height[left], height[right]) * (right - left);
            ans = Math.max(ans, curArea);
            if (height[left] <= height[right]) {
                left++;
            } else {
                right--;
            }
        }
        return ans;
    }
}
