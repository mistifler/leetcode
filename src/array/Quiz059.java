package array;

/*
59. Spiral Matrix II
Given an integer n, generate a square matrix filled with elements from 1 to n2 in spiral order.

For example,
Given n = 3,

You should return the following matrix:
[
 [ 1, 2, 3 ],
 [ 8, 9, 4 ],
 [ 7, 6, 5 ]
]
 */

public class Quiz059 {
    public int[][] generateMatrix(int n) {
        int[][] ans = new int[n][n];
        //循环遍历的边界
        int beginx = 0;
        int endx = n - 1;
        int beginy = 0;
        int endy = n - 1;
        int num = 1;

        while (true) {
            //left -> right
            for (int j = beginy; j <= endy; ++j) {
                ans[beginx][j] = num++;
            }
            //移动起始行边界，并判断是否结束
            if (++beginx > endx) break;

            //top -> bottom
            for (int i = beginx; i <= endx; ++i) {
                ans[i][endy] = num++;
            }
            if (--endy < beginy) break;

            //right -> left
            for (int j = endy; j >= beginy; --j) {
                ans[endx][j] = num++;
            }
            if (--endx < beginx) break;

            //bottom -> top
            for (int i = endx; i >= beginx; --i) {
                ans[i][beginy] = num++;
            }
            if (++beginy > endy) break;
        }
        return ans;
    }
}
