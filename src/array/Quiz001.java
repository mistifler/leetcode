package array;

import java.util.HashMap;
import java.util.Map;

/**
 * 1. Two Sum
 * Given an array of integers, return indices of the two numbers such that they add up to a specific target.
 * <p>
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 * <p>
 * Example:
 * Given nums = [2, 7, 11, 15], target = 9,
 * <p>
 * Because nums[0] + nums[1] = 2 + 7 = 9,
 * return [0, 1].
 */


public class Quiz001 {

    /**
     * 思路：HashMap空间换时间
     */
    public int[] twoSum(int[] nums, int target) {
        int[] ret = new int[2];
        Map<Integer, Integer> hash = new HashMap<>();
        for (int i = 0; i < nums.length; ++i) {
            int another = target - nums[i];
            if (hash.containsKey(another)) {
                ret[0] = hash.get(another);
                ret[1] = i;
                break;
            }
            hash.put(nums[i], i);
        }
        return ret;
    }

    public static void main(String[] args) {
        Quiz001 quiz001 = new Quiz001();
        int[] testcase = {2, 7, 11, 15};

        int[] ret = quiz001.twoSum(testcase, 9);
        System.out.println("result: " + ret[0] + "," + ret[1]);
    }
}
