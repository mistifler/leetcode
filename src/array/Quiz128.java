package array;
/*
128. Longest Consecutive Sequence
Given an unsorted array of integers, find the length of the longest consecutive elements sequence.

For example,
Given [100, 4, 200, 1, 3, 2],
The longest consecutive elements sequence is [1, 2, 3, 4]. Return its length: 4.

Your algorithm should run in O(n) complexity.
 */

import java.util.HashSet;
import java.util.Set;

public class Quiz128 {
    /**
     * 思路：HashSet
     * 需要寻找连续的元素，需要得到某元素是否存在的信息，可以用hashset来加速查找
     * 首先将所有数字存入set
     * 尝试找到连续序列的开头：即查找是否有 num-1 存在于set中，如果没有就可以以此为起点向后查找
     */
    public int longestConsecutive(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        int ans = 1;
        Set<Integer> set = new HashSet<>();

        for (int num : nums) {
            set.add(num);
        }

        for (int num : nums) {
            //如果没有紧邻这个元素的更小的值，它就是一个连续序列的开始
            if (!set.contains(num - 1)) {
                int curLen = 1;
                int curNum = num;
                while (set.contains(curNum + 1)) {
                    curNum++;
                    curLen++;
                }
                ans = Math.max(ans, curLen);
            }
        }
        return ans;
    }
}
