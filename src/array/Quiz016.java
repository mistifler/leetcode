package array;

/*
16. 3Sum Closest
Given an array S of n integers, find three integers in S such that the sum is closest to a given number, target.
Return the sum of the three integers. You may assume that each input would have exactly one solution.

For example, given array S = {-1 2 1 -4}, and target = 1.
The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
 */

import java.util.Arrays;

public class Quiz016 {
    /**
     * 思路：跟3-sum类似，不过换成了每次比较gap更新结果
     */
    public int threeSumClosest(int[] nums, int target) {
        Arrays.sort(nums);
        int ans = 0;
        int closet = Integer.MAX_VALUE;
        for (int a = 0; a < nums.length - 2; a++) {
            int b = a + 1;
            int c = nums.length - 1;
            if (a != 0 && nums[a] == nums[a - 1]) {
                continue;
            }
            while (b < c) {
                if(b != a+1 && nums[b-1] == nums[b]) {
                    b++;
                    continue;
                }
                if(c != nums.length - 1 && nums[c] == nums[c+1]) {
                    c--;
                    continue;
                }
                int curSum = nums[a] + nums[b] + nums[c];
                int gap = Math.abs(curSum - target);
                if(gap < closet) {
                    ans = curSum;
                    closet = gap;
                }
                if(curSum < target) {
                    b++;
                } else {
                    c--;
                }
            }
        }
        return ans;
    }
}
