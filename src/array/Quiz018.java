package array;
/*
18. 4Sum
Given an array S of n integers, are there elements a, b, c, and d in S such that a + b + c + d = target?
Find all unique quadruplets in the array which gives the sum of target.

Note: The solution set must not contain duplicate quadruplets.

For example, given array S = [1, 0, -1, 0, -2, 2], and target = 0.

A solution set is:
[
  [-1,  0, 0, 1],
  [-2, -1, 1, 2],
  [-2,  0, 0, 2]
]

 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Quiz018 {
    /**
     * 思路：k-sum问题
     */
    public List<List<Integer>> fourSum(int[] nums, int target) {
        List<List<Integer>> ans = new ArrayList<>();
        Arrays.sort(nums);
        for (int a = 0; a < nums.length - 3; a++) {
            if (a != 0 && nums[a] == nums[a - 1]) continue;
            for (int b = a + 1; b < nums.length - 2; b++) {
                if (b != a + 1 && nums[b] == nums[b - 1]) continue;
                int c = b + 1;
                int d = nums.length - 1;

                while (c < d) {
                    if (c != b + 1 && nums[c] == nums[c - 1]) {
                        c++;
                        continue;
                    }
                    if (d != nums.length - 1 && nums[d] == nums[d + 1]) {
                        d--;
                        continue;
                    }

                    int curSum = nums[a] + nums[b] + nums[c] + nums[d];
                    if (curSum < target) {
                        c++;
                    } else if (curSum > target) {
                        d--;
                    } else {
                        ans.add(Arrays.asList(nums[a], nums[b], nums[c], nums[d]));
                        c++;
                        d--;
                    }
                }
            }
        }
        return ans;
    }
}
