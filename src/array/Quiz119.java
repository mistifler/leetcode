package array;
/*
119. Pascal's Triangle II
Given an index k, return the kth row of the Pascal's triangle.

For example, given k = 3,
Return [1,3,3,1].

Note:
Could you optimize your algorithm to use only O(k) extra space?
 */

import java.util.ArrayList;
import java.util.List;

public class Quiz119 {
    /**
     * 思路：只要求O(k)的空间复杂度，可以考虑滚动利用当前数组
     * 相当于每次往下走需要更新两个数，同时在末尾添加一个1，比如1 2 1 -> 1 3 3 1 这步，可以理解为 把后面的 2 和1 更新成了3 3 然后在末尾添加一个1
     * 1
     * 1 1
     * 1 2 1
     * 1 3 3 1
     * 1 3 6 4 1
     */
    public List<Integer> getRow(int rowIndex) {
        List<Integer> ans = new ArrayList<>();

        for(int i=0; i<=rowIndex; i++) {
            for(int j=i-1; j>0; j--) {
                int tmp = ans.get(j) + ans.get(j-1);
                ans.set(j, tmp);
            }
            ans.add(1);
        }
        return ans;
    }
}
