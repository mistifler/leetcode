package array;

import java.util.Stack;

/*
85. Maximal Rectangle
Given a 2D binary matrix filled with 0's and 1's, find the largest rectangle containing only 1's and return its area.

For example, given the following matrix:

1 0 1 0 0
1 0 1 1 1
1 1 1 1 1
1 0 0 1 0
Return 6.
 */

public class Quiz085 {
    public int maximalRectangle(char[][] matrix) {
        if (matrix.length == 0) {
            return 0;
        }
        final int m = matrix.length;
        final int n = matrix[0].length;
        int[][] allHeights = new int[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == '0') {
                    allHeights[i][j] = 0;
                } else {
                    allHeights[i][j] = (i == 0 ? 1 : 1 + allHeights[i - 1][j]);
                }
            }
        }

        int ans = 0;
        for(int i=0; i<m; i++) {
            ans = Math.max(ans, maxArea(allHeights[i]));
        }

        return ans;
    }

    public int maxArea(int[] heights) {
        int ans = 0;
        Stack<Integer> stack = new Stack<>();
        int len = heights.length;
        for (int i = 0; i <= len; ++i) {
            int curh = (i == len ? 0 : heights[i]);
            if (stack.isEmpty() || curh > heights[stack.peek()]) {
                stack.push(i);
            } else {
                int tmph = heights[stack.pop()];
                int width = stack.isEmpty() ? i : i - stack.peek() - 1;
                ans = Math.max(ans, tmph * width);
                i--;
            }
        }
        return ans;
    }
}
