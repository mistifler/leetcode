package array;
/*
118. Pascal's Triangle
Given numRows, generate the first numRows of Pascal's triangle.

For example, given numRows = 5,
Return

[
     [1],
    [1,1],
   [1,2,1],
  [1,3,3,1],
 [1,4,6,4,1]
]
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Quiz118 {
    /**
     * 思路：递推
     * 每一行开始先把开头和结尾都赋值为1，然后其他位置递推
     *
     */
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> ans = new ArrayList<>();
        for(int i=0; i<numRows; i++) {
            Integer[] cur = new Integer[i+1];
            cur[0] = 1;
            cur[i] = 1;
            for(int j=1; j<i; j++) {
                cur[j] = ans.get(i-1).get(j-1) + ans.get(i-1).get(j);
            }
            ans.add(Arrays.asList(cur));
        }
        return ans;
    }
}
