package array;
/*
88. Merge Sorted Array
Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as one sorted array.

Note:
You may assume that nums1 has enough space (size that is greater or equal to m + n) to hold additional elements from nums2.
The number of elements initialized in nums1 and nums2 are m and n respectively.
 */

public class Quiz088 {
    /**
     * 思路：归并排序的合并部分
     * 维护三个索引，一个用于存放比较结果，另外两个用于遍历数组
     */
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int curA = m - 1;
        int curB = n - 1;
        int cur = m + n - 1;

        while(curA>=0 && curB>=0){
            if(nums1[curA]>=nums2[curB]){
                nums1[cur] = nums1[curA];
                curA--;
            }else{
                nums1[cur] = nums2[curB];
                curB--;
            }
            cur--;
        }

        while(curB>=0){
            nums1[cur] = nums2[curB];
            curB--;
            cur--;
        }
    }
}
