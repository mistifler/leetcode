package array;

/*
15. 3Sum
Given an array S of n integers, are there elements a, b, c in S such that a + b + c = 0?
Find all unique triplets in the array which gives the sum of zero.

Note: The solution set must not contain duplicate triplets.

For example, given array S = [-1, 0, 1, 2, -1, -4],

A solution set is:
[
  [-1, 0, 1],
  [-1, -1, 2]
]
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Quiz015 {
    /**
     * 思路：k-sum 问题，最内层双指针进行夹逼，外层k-2次循环
     * 每次循环的时候，如果和上个元素相同可以跳过进行剪枝
     */
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> ans = new ArrayList<>();
        if(nums.length < 3) {
            return ans;
        }
        Arrays.sort(nums);
        for(int a=0; a<nums.length -2; a++) {
            int b = a + 1;
            int c = nums.length - 1;
            if(a != 0 && nums[a] == nums[a-1]) {
                continue;
            }
            while (b < c) {
                if(b != a+1 && nums[b-1] == nums[b]) {
                    b++;
                    continue;
                }
                if(c != nums.length - 1 && nums[c] == nums[c+1]) {
                    c--;
                    continue;
                }
                int curSum = nums[a] + nums[b] + nums[c];
                if(curSum < 0) {
                    b++;
                } else if(curSum > 0) {
                    c--;
                } else {
                    ans.add(Arrays.asList(nums[a],nums[b],nums[c]));
                    b++;
                    c--;
                }
            }
        }
        return ans;
    }
}
