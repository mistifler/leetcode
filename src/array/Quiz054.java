package array;

/*
54. Spiral Matrix
Given a matrix of m x n elements (m rows, n columns), return all elements of the matrix in spiral order.

For example,
Given the following matrix:

[
 [ 1, 2, 3 ],
 [ 4, 5, 6 ],
 [ 7, 8, 9 ]
]
You should return [1,2,3,6,9,8,7,4,5].
 */

import java.util.ArrayList;
import java.util.List;

public class Quiz054 {
    /**
     * 思路：细节实现题
     * 可以定义四个界限用于界定四个方向的循环范围
     * beginx -> endx 表示行的范围
     * beginy -> endy 表示列的范围
     */
    public List<Integer> spiralOrder(int[][] matrix) {
        List<Integer> ans = new ArrayList<>();
        if (matrix.length == 0) {
            return ans;
        }

        //循环遍历的边界
        int beginx = 0;
        int endx = matrix.length - 1;
        int beginy = 0;
        int endy = matrix[0].length - 1;

        while (true) {
            //left -> right
            for (int j = beginy; j <= endy; ++j) {
                ans.add(matrix[beginx][j]);
            }
            //移动起始行边界，并判断是否结束
            if (++beginx > endx) break;

            //top -> bottom
            for (int i = beginx; i <= endx; ++i) {
                ans.add(matrix[i][endy]);
            }
            if (--endy < beginy) break;

            //right -> left
            for (int j = endy; j >= beginy; --j) {
                ans.add(matrix[endx][j]);
            }
            if (--endx < beginx) break;

            //bottom -> top
            for (int i = endx; i >= beginx; --i) {
                ans.add(matrix[i][beginy]);
            }
            if (++beginy > endy) break;
        }

        return ans;
    }

    public static void main(String[] args) {
        int[][] test = {{1,2,3}, {4,5,6}};
        Quiz054 quiz054 = new Quiz054();
        quiz054.spiralOrder(test);
    }
}
