package array;

/*
42. Trapping Rain Water
Given n non-negative integers representing an elevation map where the width of each bar is 1,
compute how much water it is able to trap after raining.

For example,
Given [0,1,0,2,1,0,1,3,2,1,2,1], return 6.
The above elevation map is represented by array [0,1,0,2,1,0,1,3,2,1,2,1]. In this case,
6 units of rain water (blue section) are being trapped. Thanks Marcos for contributing this image!
 */

public class Quiz042 {
    /**
     * 思路：双指针
     * 维护一个left指针，一个right指针，直到两者相遇
     * 维护一个leftMax和一个rightMax，记录遍历遇到过的最大值
     * 每次移动值较小的一端，因为盛水量是由小的决定的，
     * 以左边小为例子，左边能否蓄水不需要考虑自己的右边界，因为肯定有比自己高的，这样只需要关注自己是否有leftMax高即可
     * 如果 < leftMax 表示可以蓄水，否则更新leftMax
     */
    public int trap(int[] height) {
        int left = 0;
        int right = height.length - 1;
        int leftMax = 0;
        int rightMax = 0;
        int ans = 0;
        while (left < right) {
            if (height[left] < height[right]) {
                if(height[left] >= leftMax) {
                    leftMax = height[left];
                } else {
                    ans += leftMax - height[left];
                }
                ++left;
            } else {
                if(height[right] >= rightMax) {
                    rightMax = height[right];
                } else {
                    ans += rightMax - height[right];
                }
                --right;
            }
        }
        return ans;
    }
}
