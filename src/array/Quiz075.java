package array;
/*
75. Sort Colors
Given an array with n objects colored red, white or blue, sort them so that objects of the same color are adjacent, with the colors in the order red, white and blue.

Here, we will use the integers 0, 1, and 2 to represent the color red, white, and blue respectively.

Note:
You are not suppose to use the library's sort function for this problem.

click to show follow up.

Follow up:
A rather straight forward solution is a two-pass algorithm using counting sort.
First, iterate the array counting number of 0's, 1's, and 2's, then overwrite array with total number of 0's, then 1's and followed by 2's.

Could you come up with an one-pass algorithm using only constant space?
 */

public class Quiz075 {
    /**
     * 思路：双指针
     * idx0表示在idx0之前都已经全部为0，idx2表示在idx2之后全部为2.
     * curIdx进行遍历，当curIdx <= idx2的时候
     * 如果当前元素为0，将其与idx0交换，idx0++
     * 然后接着判断curIdx是否为2，如果是与idx2交换,idx2--，同时curIdx--,保证下次循环依然要考察缓过来的元素
     * 继续curIdx++
     */
    public void sortColors(int[] nums) {
        int idx0 = 0;
        int idx2 = nums.length - 1;
        int curIdx = 0;
        while (curIdx <= idx2) {
            //如果是0，直接交换
            if(nums[curIdx] == 0) {
                nums[curIdx] = nums[idx0];
                nums[idx0] = 0;
                idx0++;
            }
            //可能是上面交换过来的1，也可能本身该元素就是2
            if(nums[curIdx] == 2) {
                nums[curIdx] = nums[idx2];
                nums[idx2] = 2;
                idx2--;
                //下次循环还要考察当前元素
                curIdx--;
            }
            curIdx++;
        }
    }
}
