package array;

/*
45. Jump Game II
Given an array of non-negative integers, you are initially positioned at the first index of the array.

Each element in the array represents your maximum jump length at that position.

Your goal is to reach the last index in the minimum number of jumps.

For example:
Given array A = [2,3,1,1,4]

The minimum number of jumps to reach the last index is 2. (Jump 1 step from index 0 to 1, then 3 steps to the last index.)

Note:
You can assume that you can always reach the last index.
 */

import java.util.Map;

public class Quiz045 {
    /**
     * 思路：贪心
     * 理论上任意位置都是可达的，所以遍历过程中维护两个变量 farPos 表示最远可以到达的位置，lastPos表示上次需要跳一次到达的最远位置
     */
    public int jump(int[] nums) {
        int ans = 0;
        int farPos = 0;
        int lastPos = 0;
        for(int i=0; i<nums.length; ++i) {
            if(i > lastPos) {
                lastPos = farPos;
                ans++;
            }
            farPos = Math.max(farPos, nums[i] + i);
        }
        return ans;
    }
}
