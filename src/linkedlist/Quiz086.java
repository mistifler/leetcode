package linkedlist;

/*
86. Partition List
Given a linked list and a value x, partition it such that all nodes less than x come before nodes greater than or equal to x.

You should preserve the original relative order of the nodes in each of the two partitions.

For example,
Given 1->4->3->2->5->2 and x = 3,
return 1->2->2->4->3->5.
 */

public class Quiz086 {
    public ListNode partition(ListNode head, int x) {
        if (head == null || head.next == null) {
            return head;
        }

        //初始化虚拟头结点和tag指针，cur遍历过程中发现满足条件的元素都放到tag节点后
        ListNode dummy = new ListNode(-1);
        dummy.next = head;
        ListNode tag = dummy;

        ListNode prev = dummy;
        ListNode cur = dummy.next;
        while (cur != null) {
            if (cur.val < x) {
                //如果待删除节点的prev和tag一致就没必要执行删除操作，所有的指针一致后移即可
                if (tag == prev) {
                    tag = tag.next;
                    prev = prev.next;
                    cur = cur.next;
                    continue;
                }
                //将cur节点从prev处删除，并插入到tag后，完成后tag指针后移
                prev.next = cur.next;
                cur.next = tag.next;
                tag.next = cur;

                //tag和cur指针后移
                tag = tag.next;
                cur = prev.next;
            } else {
                prev = prev.next;
                cur = cur.next;
            }
        }
        return dummy.next;
    }
}
