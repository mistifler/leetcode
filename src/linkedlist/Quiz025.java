package linkedlist;

/*
25. Reverse Nodes in k-Group
Given a linked list, reverse the nodes of a linked list k at a time and return its modified list.

k is a positive integer and is less than or equal to the length of the linked list. If the number of nodes is not a multiple of k then left-out nodes in the end should remain as it is.

You may not alter the values in the nodes, only nodes itself may be changed.

Only constant memory is allowed.

For example,
Given this linked list: 1->2->3->4->5

For k = 2, you should return: 2->1->4->3->5

For k = 3, you should return: 3->2->1->4->5
 */

public class Quiz025 {
    public ListNode reverseKGroup(ListNode head, int k) {
        if(k == 1 || head == null || head.next == null) {
            return head;
        }

        ListNode dummy = new ListNode(-1);
        dummy.next = head;

        //每一组的head
        ListNode tmpHead = dummy;
        //end 每次移动k个位置
        ListNode end = dummy.next;

        while (end != null) {
            //每一组的结尾
            for(int i=0; i<k-1 && end != null; i++) {
                end = end.next;
            }

            //如果不够k个直接返回
            if(end == null) {
                break;
            }

            //反转并获取下一组的头结点
            tmpHead = reverseHelper(tmpHead, tmpHead.next, end);
            //移动end指针
            end = tmpHead.next;
        }

        return dummy.next;
    }

    //tmpHead -> start -> .... -> end 的 start到end部分的节点反转，并返回最后一个节点（下一组的头结点）
    public ListNode reverseHelper(ListNode tmpHead, ListNode start, ListNode end) {

        ListNode endNext = end.next;

        //cur从start的下一个节点开始，依次插入到tmpHead和start之间
        ListNode cur = start.next;
        while (cur != endNext) {
            start.next = cur.next;
            //cur插入到head节点之后
            cur.next = tmpHead.next;
            tmpHead.next = cur;
            //后移遍历
            cur = start.next;
        }

        //返回start，可以作为下一组的头节点(tmpHead)
        return start;
    }
}
