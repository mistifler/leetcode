package linkedlist;

/*
206. Reverse Linked List
Reverse a singly linked list.

click to show more hints.

Hint:
A linked list can be reversed either iteratively or recursively. Could you implement both?
 */

public class Quiz206 {
    /**
     * 思路1：
     * 遍历所有节点，维护 prev cur curNext 三个指针，cur用于遍历，prev表示之前已经反转好的链表的头结点，curNext用于暂存下个需要反转的节点
     *
     * ( <- prev cur -> curNext)  =>  ( <- prev <- cur curNext)
     */
    public ListNode reverseList(ListNode head) {

        // 当前需要反转的指针，即需要将其next节点指向其之前的节点
        ListNode cur = head;
        //在修改cur的next指针之前先保存下一个需要修改的Node
        ListNode curNext = null;
        //此节点为反转后的新的头结点
        ListNode prev = null;

        while (cur != null) {
            //保存下个节点位置
            curNext = cur.next;
            //当前节点反转
            cur.next = prev;

            //移动指针
            prev = cur;
            cur = curNext;
        }
        return prev;
    }

    /**
     * 思路2： 递归
     * 结束条件：节点为null或者节点只有一个，返回该节点即可。
     * 将head.next 后面的节点调用递归函数反转得到一个新的节点newHead，此时head -> end <- ... <- newHead，修正head的指向关系即可
     *
     */
    public ListNode reverseList2(ListNode head) {
        if(head == null || head.next == null) {
            return head;
        } else {
            ListNode newHead = reverseList2(head.next);
            head.next.next = head;
            head.next = null;
            return newHead;
        }
    }
}
