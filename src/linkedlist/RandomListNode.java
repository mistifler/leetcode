package linkedlist;

/**
 * Created by renshengmin on 18/2/12.
 */
public class RandomListNode {
    int label;
    RandomListNode next, random;

    RandomListNode(int x) {
        this.label = x;
    }
}
