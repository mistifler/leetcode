package linkedlist;

/*
92. Reverse Linked List II
Reverse a linked list from position m to n. Do it in-place and in one-pass.

For example:
Given 1->2->3->4->5->NULL, m = 2 and n = 4,

return 1->4->3->2->5->NULL.

Note:
Given m, n satisfy the following condition:
1 ≤ m ≤ n ≤ length of list.

 */

public class Quiz092 {
    /**
     * 思路：链表反转，和LeetCode25类似，可以考虑固定区间前的一个头结点，然后依次遍历删除并重新插入到这个头结点之后
     * tmpHead -> start -> ... -> end，cur从start.next开始，每次插入到tmpHead之后
     */
    public ListNode reverseBetween(ListNode head, int m, int n) {
        ListNode dummy = new ListNode(-1);
        dummy.next = head;

        //找到待反转区间的头结点
        ListNode tmpHead = dummy;
        for(int i=0; i<m-1; i++) {
            tmpHead = tmpHead.next;
        }

        //cur依次遍历插入到tmpHea之后，start每次指向cur.next即可以缓存下个遍历的位置，而且作为反转后的最后一个节点，可以正确指向区间外的第一个节点
        ListNode start = tmpHead.next;
        ListNode cur = start.next;
        for(int i=m; i<n; i++) {
            //cur的下个位置，以及遍历完可以指向正确的位置
            start.next = cur.next;
            //删除并插入
            cur.next = tmpHead.next;
            tmpHead.next = cur;
            //cur后移遍历
            cur = start.next;
        }
        return dummy.next;
    }
}
