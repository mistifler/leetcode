package linkedlist;

/*
83. Remove Duplicates from Sorted List
Given a sorted linked list, delete all duplicates such that each element appear only once.

For example,
Given 1->1->2, return 1->2.
Given 1->1->2->3->3, return 1->2->3.

 */

public class Quiz083 {
    public ListNode deleteDuplicates(ListNode head) {
        if(head == null || head.next == null) {
            return head;
        }

        ListNode dummy = new ListNode(-1);
        dummy.next = head;

        //每次比较cur和cmp，如果cmp和cur一样就删除它，否则一起往后移动
        ListNode cur = head;
        ListNode cmp = cur.next;
        while (cmp != null) {
            if(cmp.val == cur.val) {
                cmp = cmp.next;
                cur.next = cmp;
            } else {
                cur = cur.next;
                cmp = cmp.next;
            }
        }
        return dummy.next;
    }
}
